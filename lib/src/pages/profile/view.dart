import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import '../../datas/models/user.dart';
import '../../values/index.dart';
import 'controller.dart';

class ProfilePage extends StatelessWidget {
  ProfilePage({Key? key}) : super(key: key);

  final controller = Get.find<ProfileController>();

  Widget _buildAvatar(User user) {
    return Card(
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: InkWell(
        borderRadius: Radii.k6px,
        onTap: () {},
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
          child: SizedBox(
            width: 355.w,
            height: 130.h,
            child: Row(
              children: [
                CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(
                    user.avatar,
                  ),
                  minRadius: 45,
                  maxRadius: 70,
                ),
                SizedBox(
                  width: 20.w,
                ),
                Text(
                  user.name,
                  style:
                      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildListTile(
      {required String title,
      required VoidCallback onTap,
      required IconData icon}) {
    return Card(
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: SizedBox(
        width: 355.w,
        height: 60.h,
        child: InkWell(
          borderRadius: Radii.k6px,
          onTap: onTap,
          child: Row(
            children: [
              SizedBox(
                width: 10.w,
              ),
              Icon(
                icon,
                size: 24.w,
              ),
              SizedBox(
                width: 10.w,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 13.sp,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSettingList() {
    return Column(
      children: [
        _buildListTile(
          title: settingTitles[0],
          onTap: () {
            controller.handleNavUserDetail();
          },
          icon: Icons.person_outlined,
        ),
        _buildListTile(
          title: settingTitles[1],
          onTap: () {
            controller.handleNavAchievement();
          },
          icon: Icons.emoji_events_outlined,
        ),
        _buildListTile(
          title: settingTitles[2],
          onTap: () {
            controller.handleNavRecall();
          },
          icon: Icons.notifications_active_outlined,
        ),
        _buildListTile(
          title: settingTitles[3],
          onTap: () {},
          icon: Icons.info_outlined,
        ),
        _buildListTile(
          title: settingTitles[4],
          onTap: () {
            controller.handleLogout();
          },
          icon: Icons.logout_outlined,
        ),
      ],
    );
  }

  List<String> settingTitles = [
    "Thông tin cá nhân",
    "Xem thành tích",
    "Nhắc lại từ vựng đã học",
    "Về chúng tôi",
    "Đăng xuất"
  ];

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ProfileController>();

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              controller.user.value != null
                  ? _buildAvatar(controller.user.value!)
                  : spinKit(),
              SizedBox(
                height: 10.h,
              ),
              _buildSettingList()
            ]),
          ),
        ),
      ),
    );
  }
}
