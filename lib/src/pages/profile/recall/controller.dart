import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/networks/user_request.dart';
import 'package:memolary_mobile/src/datas/providers/user_provider.dart';
import 'package:memolary_mobile/src/pages/profile/controller.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import 'index.dart';

class RecallController extends GetxController {
  final recallWords = "".obs;
  final recallTime = "".obs;

  final ProfileController profileController = Get.find();

  @override
  void onInit() {
    prepareData();
    super.onInit();
  }

  prepareData() {
    print("prepare ${profileController.user.value!.recallTime}");
    if (profileController.user.value!.recallWords == 0) {
      recallWords.value = "".toString();
    } else {
      recallWords.value = profileController.user.value!.recallWords.toString();
    }
    recallTime.value = profileController.user.value!.recallTime;
    print(recallWords.value);
    print(recallTime.value);
  }

  handleQuestion() {
    toastInfo(
        msg: "Thời gian thích hợp nhất để xem lại từ vựng là vào buổi sáng");
  }

  handleConfirmRecall() async {
    try {
      if (recallWords.value == "") {
        await UserProvider().updateRecall(
          RecallRequest(recallWords: 0, recallTime: recallTime.value),
        );
      } else {
        await UserProvider().updateRecall(
          RecallRequest(
              recallWords: int.parse(recallWords.value),
              recallTime: recallTime.value),
        );
      }

      await profileController.fetchUser();
      toastInfo(msg: "Thiết lập thành công");
    } catch (e) {
      print(e);
    }
  }
}
