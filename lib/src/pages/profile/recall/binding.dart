import 'package:get/get.dart';

import 'controller.dart';

class RecallBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RecallController());
  }
}
