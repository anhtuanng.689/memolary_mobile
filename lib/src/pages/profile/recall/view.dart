import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class RecallPage extends StatelessWidget {
  RecallPage({Key? key}) : super(key: key);

  final controller = Get.find<RecallController>();

  Widget _buildRecall() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10.h,
        ),
        Row(
          children: [
            Text(
              "Số từ vựng được nhắc lại",
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
            ),
            const Spacer(),
            SizedBox(
              width: 60.w,
              child: DropdownButton<String>(
                isExpanded: true,
                value: controller.recallWords.value,
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  size: 18.w,
                ),
                elevation: 16,
                style: TextStyle(fontSize: 16.sp, color: AppColors.primaryText),
                underline: Container(
                  height: 2.h,
                  color: AppColors.primaryElement,
                ),
                onChanged: (String? newValue) {
                  controller.recallWords.value = newValue!;
                },
                items: <String>['', '1', '2', '3', '4', '5']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            SizedBox(
              width: 30.w,
            ),
          ],
        ),
        SizedBox(
          height: 10.h,
        ),
        Row(
          children: [
            Text(
              "Thời điểm nhắc lại",
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
            ),
            const Spacer(),
            SizedBox(
              width: 60.w,
              child: DropdownButton<String>(
                isExpanded: true,
                value: controller.recallTime.value,
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  size: 18.w,
                ),
                elevation: 16,
                style: TextStyle(fontSize: 16.sp, color: AppColors.primaryText),
                underline: Container(
                  height: 2.h,
                  color: AppColors.primaryElement,
                ),
                onChanged: (String? newValue) {
                  controller.recallTime.value = newValue!;
                },
                items: <String>[
                  '',
                  '00:00', '01:00', '02:00', '03:00', '04:00',
                  '05:00',
                  // '05:30',
                  '06:00',
                  // '06:30',
                  '07:00',
                  // '07:30',
                  '08:00', '09:00', '10:00', '11:00',
                  '12:00', '13:00', '14:00', '15:00',
                  '16:00', '17:00', '18:00', '19:00',
                  '20:00', '21:00', '22:00', '23:00',
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            SizedBox(
              width: 30.w,
            ),
          ],
        ),
        SizedBox(
          height: 10.h,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<RecallController>();

    return SafeArea(
      child: Scaffold(
        appBar: transparentAppBar(
          title: "Thiết lập nhắc lại từ vựng",
          actions: [
            IconButton(
              onPressed: () {
                controller.handleQuestion();
              },
              icon: Icon(
                Icons.help_outline_outlined,
                size: 18.w,
                color: AppColors.thirdElement,
              ),
            ),
          ],
        ),
        floatingActionButton: btnFlatButtonWidget(
          onPressed: () {
            controller.handleConfirmRecall();
          },
          width: 325.w,
          fontWeight: FontWeight.w600,
          title: "Xác nhận",
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: Obx(() {
              return _buildRecall();
            }),
          ),
        ),
      ),
    );
  }
}
