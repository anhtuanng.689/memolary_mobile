import 'package:get/get.dart';

import 'controller.dart';

class UserDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserDetailController());
  }
}
