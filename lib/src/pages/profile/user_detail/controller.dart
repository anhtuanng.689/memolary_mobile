import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/pages/profile/controller.dart';
import 'package:intl/intl.dart';

import '../../../utils/index.dart';
import 'index.dart';

class UserDetailController extends GetxController {
  final sexRadio = "Nam".obs;

  final TextEditingController nameInputController = TextEditingController();
  final TextEditingController emailInputController = TextEditingController();
  final TextEditingController phoneInputController = TextEditingController();
  final TextEditingController birthInputController = TextEditingController();

  final profileController = Get.find<ProfileController>();

  @override
  void onInit() {
    nameInputController.text = profileController.user.value!.name;
    emailInputController.text = profileController.user.value!.email;
    phoneInputController.text = profileController.user.value!.phone;
    birthInputController.text = DateFormat("yyyy-MM-dd")
        .format(DateTime.parse(profileController.user.value!.birthday));

    super.onInit();
  }
}
