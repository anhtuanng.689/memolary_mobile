import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/widgets/app.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import '../../../values/index.dart';
import 'controller.dart';

class UserDetailPage extends StatelessWidget {
  UserDetailPage({Key? key}) : super(key: key);
  final controller = Get.find<UserDetailController>();

  Widget _buildUserDetail() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10.h,
        ),
        Text(
          "Tên hiển thị",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
        ),
        inputCardEdit(
          controller: controller.nameInputController,
          hintText: "Tên hiển thị",
          fontSize: 16,
        ),
        SizedBox(
          height: 10.h,
        ),
        Text(
          "Địa chỉ email",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
        ),
        AbsorbPointer(
          child: inputCardEdit(
            color: Colors.grey.shade300,
            controller: controller.emailInputController,
            hintText: "Email",
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        Text(
          "Số điện thoại",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
        ),
        inputCardEdit(
          controller: controller.phoneInputController,
          hintText: "0XXXXXXXXX",
          fontSize: 16,
        ),
        SizedBox(
          height: 10.h,
        ),
        Text(
          "Ngày sinh",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
        ),
        inputCardEdit(
          controller: controller.birthInputController,
          hintText: "dd/MM/yyyy",
          fontSize: 16,
        ),
        SizedBox(
          height: 10.h,
        ),
        Text(
          "Giới tính",
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
        ),
        SizedBox(
          height: 10.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: ListTile(
                title: Text(
                  "Nam",
                  style: TextStyle(fontSize: 16.sp),
                ),
                leading: Radio<String>(
                  value: 'Nam',
                  groupValue: controller.sexRadio.value,
                  onChanged: (value) {
                    controller.sexRadio.value = value!;
                  },
                  activeColor: Colors.green,
                ),
              ),
            ),
            Flexible(
              child: ListTile(
                title: Text(
                  "Nữ",
                  style: TextStyle(fontSize: 16.sp),
                ),
                leading: Radio<String>(
                  value: "Nữ",
                  groupValue: controller.sexRadio.value,
                  onChanged: (value) {
                    controller.sexRadio.value = value!;
                  },
                  activeColor: Colors.green,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<UserDetailController>();

    return SafeArea(
      child: Scaffold(
        appBar: transparentAppBar(
          title: "Thông tin cá nhân",
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.more_vert,
                size: 18.w,
                color: AppColors.thirdElement,
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: Obx(() {
              return _buildUserDetail();
            }),
          ),
        ),
      ),
    );
  }
}
