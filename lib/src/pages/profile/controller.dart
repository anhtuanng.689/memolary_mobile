import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/providers/user_provider.dart';
import 'package:memolary_mobile/src/routes/app_routes.dart';

import '../../datas/models/user.dart';
import '../../datas/networks/user_request.dart';
import '../../datas/providers/network_exception.dart';
import '../../values/index.dart';

class ProfileController extends GetxController {
  final user = Rx<User?>(null);

  @override
  void onInit() {
    fetchUser();
    super.onInit();
  }

  fetchUser() async {
    try {
      // print("fetching user...");
      user.value = await UserProvider().getProfile();
      return user.value;
    } on UnauthorisedException catch (e) {
      print(e);
    } catch (e) {
      print(e);
    }
  }

  handleLogout() async {
    await UserProvider().updateUser(UserRequest(fcmToken: ""));

    Storage().removeIsLogined();
    Storage().removeAccessToken();

    Get.offAndToNamed(AppRoutes.signIn);
  }

  handleNavUserDetail() {
    Get.toNamed(AppRoutes.userDetail);
  }

  handleNavAchievement() {
    Get.toNamed(AppRoutes.achievement);
  }

  handleNavRecall() {
    Get.toNamed(AppRoutes.recall);
  }
}
