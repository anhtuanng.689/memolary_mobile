import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/pages/profile/controller.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import '../../review/review/index.dart';
import 'controller.dart';

class AchievementPage extends StatelessWidget {
  AchievementPage({Key? key}) : super(key: key);

  final controller = Get.find<AchievementController>();
  final profileController = Get.find<ProfileController>();
  final reviewController = Get.find<ReviewController>();

  Widget _buildAchievementCard({
    required VoidCallback onPressed,
    double width = 355,
    double height = 120,
    required String name,
    required String image,
    required int number,
    required String content,
  }) {
    return Card(
      elevation: 3,
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: SizedBox(
        width: width.w,
        height: height.h,
        child: InkWell(
          borderRadius: Radii.k6px,
          onTap: onPressed,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 10.h),
            child: Row(
              children: [
                SizedBox(
                  width: 10.w,
                ),
                Flexible(
                  flex: 2,
                  child: netImageCached(
                    image,
                    fit: BoxFit.scaleDown,
                  ),
                ),
                SizedBox(
                  width: 15.w,
                ),
                Flexible(
                  flex: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                            fontSize: 16.sp, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Text(
                        "$number $content",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAchievement() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 5.h,
        ),
        _buildAchievementCard(
          onPressed: () {},
          name: 'Cảm ơn bạn đã đồng hành cùng Memolary',
          image: "https://cdn-icons-png.flaticon.com/512/4269/4269940.png",
          content: 'ngày',
          number: DateTime.now()
                      .difference(DateTime.parse(
                          profileController.user.value!.birthday))
                      .inDays ==
                  0
              ? 1
              : DateTime.now()
                  .difference(
                      DateTime.parse(profileController.user.value!.birthday))
                  .inDays,
        ),
        SizedBox(
          height: 5.h,
        ),
        _buildAchievementCard(
          onPressed: () {},
          name: 'Các hộp từ vựng của bạn chứa',
          image: "https://cdn-icons-png.flaticon.com/512/1694/1694364.png",
          content: 'từ vựng',
          number: reviewController.countWords[0] +
              reviewController.countWords[1] +
              reviewController.countWords[2] +
              reviewController.countWords[3] +
              reviewController.countWords[4],
        ),
        SizedBox(
          height: 5.h,
        ),
        _buildAchievementCard(
          onPressed: () {},
          name: 'Số từ vựng ghi nhớ dài hạn của bạn',
          image: "https://cdn-icons-png.flaticon.com/512/1535/1535019.png",
          content: 'từ vựng',
          number:
              reviewController.countWords[3] + reviewController.countWords[4],
        ),
        SizedBox(
          height: 5.h,
        ),
        _buildAchievementCard(
          onPressed: () {},
          name: 'Bạn đã học tập liên tục trong',
          image: "https://cdn-icons-png.flaticon.com/512/861/861506.png",
          content: 'ngày',
          number: 1,
        ),
        SizedBox(
          height: 5.h,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<AchievementController>();

    return SafeArea(
      child: Scaffold(
        appBar: transparentAppBar(
          title: "Thành tích của tôi",
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.more_vert,
                size: 18.w,
                color: AppColors.thirdElement,
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: _buildAchievement(),
          ),
        ),
      ),
    );
  }
}
