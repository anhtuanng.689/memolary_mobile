import 'package:get/get.dart';

import 'controller.dart';

class AchievementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AchievementController());
  }
}
