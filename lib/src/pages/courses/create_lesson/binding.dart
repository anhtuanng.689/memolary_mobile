import 'package:get/get.dart';

import 'controller.dart';

class CreateLessonBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateLessonController());
  }
}
