import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class CreateLessonPage extends StatelessWidget {
  CreateLessonPage({Key? key}) : super(key: key);

  final controller = Get.find<CreateLessonController>();

  Widget _buildLessonName() {
    return inputCardEdit(
      controller: controller.lessonInputController,
      hintText: "Tên bài học",
      fontSize: 16,
    );
  }

  _buildListWordInput() {
    return controller.listCount
        .map((e) => _buildWordInput(e, () => controller.handleRemoveCell(e)));
  }

  Widget _buildWordInput(int num, VoidCallback onPressed) {
    return Card(
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "Từ vựng ${num + 1}",
                  style:
                      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                ),
                const Spacer(),
                IconButton(
                  onPressed: onPressed,
                  icon: Icon(
                    Icons.clear,
                    size: 18.w,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Center(
                    child: SizedBox(
                      height: 150.h,
                      width: 130.w,
                      child: controller.isUploadList[num]
                          ? Image.file(controller.imageList[num])
                          : Material(
                              borderRadius: Radii.k6px,
                              color: AppColors.tabBarElement,
                              child: InkWell(
                                borderRadius: Radii.k6px,
                                highlightColor: Colors.grey.withOpacity(0.4),
                                splashColor: Colors.grey.withOpacity(0.5),
                                onTap: () {
                                  controller.handlePickImage(num);
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.camera_alt,
                                      size: 21.w,
                                      color: AppColors.thirdElement,
                                    ),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Text(
                                      "Tải ảnh lên",
                                      style: TextStyle(fontSize: 13.sp),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      inputTextEditWithBorder(
                          controller: controller.wordController[num],
                          hintText: "Từ vựng",
                          maxLines: 2,
                          onChanged: (value) {
                            controller.handleGetPronounce(value, num);
                          }),
                      inputTextEditWithBorder(
                        isPhonetic: true,
                        controller: controller.pronounceController[num],
                        hintText: "Phiên âm",
                        maxLines: 2,
                      ),
                      inputTextEditWithBorder(
                        controller: controller.wordMeanController[num],
                        hintText: "Nghĩa",
                        maxLines: 2,
                      ),
                    ],
                  ),
                )
              ],
            ),
            inputTextEditWithBorder(
              controller: controller.sentenceController[num],
              hintText: "Câu",
              maxLines: 2,
            ),
            inputTextEditWithBorder(
              controller: controller.sentenceMeanController[num],
              hintText: "Nghĩa của câu",
              maxLines: 2,
            ),
            SizedBox(
              height: 10.h,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<CreateLessonController>();

    return SafeArea(
      child: Obx(() {
        return Scaffold(
          appBar: transparentAppBar(
            title: "Tạo bài học",
            actions: [
              IconButton(
                onPressed: () {
                  controller.handleCreateLesson();
                },
                icon: Icon(
                  Icons.done,
                  size: 20.w,
                  color: AppColors.thirdElement,
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: AppColors.primaryElement,
            child: Icon(
              Icons.add,
              size: 20.w,
            ),
            onPressed: () {
              controller.handleAddCell();

              print(controller.listCount);
            },
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildLessonName(),
                  ..._buildListWordInput(),
                  SizedBox(
                    height: 10.h,
                  ),
                  SizedBox(
                    height: 300.h,
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
