import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memolary_mobile/src/datas/networks/lesson_request.dart';
import 'package:memolary_mobile/src/pages/courses/course/controller.dart';
import 'package:memolary_mobile/src/pages/courses/courses/controller.dart';
import 'package:memolary_mobile/src/pages/discover/controller.dart';
import '../../../datas/networks/word_request.dart';
import '../../../datas/providers/lesson_provider.dart';
import '../../../utils/index.dart';
import '../../../values/index.dart';
import '../../../widgets/index.dart';

class CreateLessonController extends GetxController {
  final ImagePicker imagePicker = ImagePicker();
  final debounce = Debounce(const Duration(milliseconds: 500));

  final isLoading = false.obs;

  final isUploadList = [false].obs;
  final imageValueList = [""].obs;
  final imageList = [File("")].obs;
  final imageLink = [""].obs;

  final course = Get.arguments[0] as String;

  final listCount = [0].obs;

  final words = <WordRequest>[].obs;

  final TextEditingController lessonInputController = TextEditingController();

  final List<TextEditingController> wordController =
      [TextEditingController()].obs;
  final List<TextEditingController> pronounceController =
      [TextEditingController()].obs;
  final List<TextEditingController> wordMeanController =
      [TextEditingController()].obs;
  final List<TextEditingController> sentenceController =
      [TextEditingController()].obs;
  final List<TextEditingController> sentenceMeanController =
      [TextEditingController()].obs;

  final CourseController courseController = Get.find();

  final CoursesController coursesController = Get.find();

  final DiscoverController discoverController = Get.find();

  @override
  void dispose() {
    lessonInputController.dispose();
    wordController.map((e) => e.dispose());
    pronounceController.map((e) => e.dispose());
    wordMeanController.map((e) => e.dispose());
    sentenceController.map((e) => e.dispose());
    sentenceMeanController.map((e) => e.dispose());
    super.dispose();
  }

  handlePickImage(int index) async {
    XFile? imageTemp = await imagePicker.pickImage(source: ImageSource.gallery);
    if (imageTemp != null) {
      isLoading.value = true;
      Loading.show();
      imageValueList[index] = imageTemp.path;
      isUploadList[index] = true;
      imageList[index] = File(imageValueList[index]);
      final ref = FirebaseStorage.instance.ref().child(
          "$storagePath${imageValueList[index]}${Random().nextInt(99999)}");
      UploadTask uploadTask = ref.putFile(imageList[index]);
      final snapshot = await uploadTask.whenComplete(() => {});

      final urlDownload = await snapshot.ref.getDownloadURL();
      print("Link: $urlDownload");
      imageLink[index] = urlDownload;
      if (imageLink[index] != "") {
        isLoading.value = false;
        Loading.dismiss();
      }
    }
  }

  handleAddCell() {
    listCount.add(listCount.length);

    isUploadList.add(false);
    imageValueList.add("");
    imageList.add(File(""));
    imageLink.add("");

    wordController.add(TextEditingController());
    pronounceController.add(TextEditingController());
    wordMeanController.add(TextEditingController());
    sentenceController.add(TextEditingController());
    sentenceMeanController.add(TextEditingController());
  }

  handleGetPronounce(String word, int index) async {
    debounce.call(() async {
      try {
        String result = "";
        List<String> list = word.split(" ");
        print(list);
        for (String str in list) {
          String temp = await LessonProvider().getPronunciation(str);
          result += temp + " ";
        }
        pronounceController[index].text = result.trim();
        print(result);
      } catch (e) {
        pronounceController[index].text = "";
        print(e);
      }
    });
  }

  handleRemoveCell(int index) {
    listCount.removeAt(listCount.length - 1);

    isUploadList.removeAt(index);
    imageValueList.removeAt(index);
    imageList.removeAt(index);
    imageLink.removeAt(index);

    wordController.removeAt(index);
    pronounceController.removeAt(index);
    wordMeanController.removeAt(index);
    sentenceController.removeAt(index);
    sentenceMeanController.removeAt(index);
  }

  handleCreateLesson() async {
    FocusManager.instance.primaryFocus?.unfocus();
    words.clear();

    String lesson = lessonInputController.value.text;
    if (lesson.isEmpty) {
      toastInfo(msg: 'Vui lòng điền tên bài học');
      return;
    }

    if (wordController.length < 4) {
      toastInfo(msg: 'Vui lòng tạo bài học với ít nhất 5 từ vựng');
      return;
    }
    print(lesson);

    for (var index in listCount) {
      if (imageLink[index] == "") {
        toastInfo(
            msg:
                'Hệ thống đã xử lý ảnh của bạn. Vui lòng thử lại trong giây lát...');
        return;
      }
      if (wordController[index].value.text.isEmpty ||
          wordMeanController[index].value.text.isEmpty ||
          sentenceController[index].value.text.isEmpty ||
          sentenceMeanController[index].value.text.isEmpty ||
          imageValueList[index] == "") {
        toastInfo(msg: 'Vui lòng điền đủ các trường của từ vựng ${index + 1}');
        return;
      }
      var word = WordRequest(
          word: wordController[index].value.text,
          pronounce: pronounceController[index].value.text == ""
              ? " "
              : pronounceController[index].value.text,
          wordMeaning: wordMeanController[index].value.text,
          sentence: sentenceController[index].value.text,
          sentenceMeaning: sentenceMeanController[index].value.text,
          image: imageLink[index]);
      words.add(word);
    }

    try {
      // Loading.show();
      await LessonProvider().createLesson(
        LessonRequest(name: lesson, course: course, words: words),
      );
      // Loading.dismiss();
      await courseController.fetchCourse(courseId: course);
      await coursesController.fetchMyCourseList();
      await coursesController.fetchRecentHistory();
      await discoverController.fetchAllCourses();
      toastInfo(msg: "Tạo bài học thành công");
      Get.back();
    } catch (e) {
      print(e);
    }
  }
}
