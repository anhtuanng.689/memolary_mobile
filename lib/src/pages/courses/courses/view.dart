import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import '../../profile/index.dart';
import 'controller.dart';

class CoursesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.find<CoursesController>();

    Widget _buildCreateCourse() {
      return btnFlatButtonWidget(
        onPressed: () {
          print(controller.myCourseList);
          controller.handleResetDialog();
          // showDialog(
          //   context: context,
          //   builder: (_) => SimpleDialog(
          //       contentPadding:
          //           EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
          //       elevation: 10,
          //       title: Center(
          //         child: Text(
          //           "Tạo khoá học",
          //           style:
          //               TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w600),
          //         ),
          //       ),
          //       children: [
          //         Column(
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             SizedBox(
          //               height: 10.h,
          //             ),
          //             Text(
          //               "Tên khoá học",
          //               style: TextStyle(
          //                   fontSize: 13.sp, fontWeight: FontWeight.w600),
          //             ),
          //             inputTextEdit(
          //                 controller: controller.coursesController,
          //                 hintText: "Lớp 12, TOEIC...",
          //                 fontSize: 13.sp),
          //             SizedBox(
          //               height: 10.h,
          //             ),
          //             Obx(
          //               () {
          //                 return Center(
          //                   child: SizedBox(
          //                     height: 150.h,
          //                     width: 130.w,
          //                     child: controller.isUpload.value
          //                         ? Image.file(controller.image)
          //                         : Material(
          //                             borderRadius: Radii.k6px,
          //                             color: AppColors.tabBarElement,
          //                             child: InkWell(
          //                               borderRadius: Radii.k6px,
          //                               highlightColor:
          //                                   Colors.grey.withOpacity(0.4),
          //                               splashColor:
          //                                   Colors.grey.withOpacity(0.5),
          //                               onTap: () {
          //                                 controller.handlePickImage();
          //                               },
          //                               child: Column(
          //                                 mainAxisAlignment:
          //                                     MainAxisAlignment.center,
          //                                 crossAxisAlignment:
          //                                     CrossAxisAlignment.center,
          //                                 children: [
          //                                   Icon(
          //                                     Icons.camera_alt,
          //                                     size: 21.w,
          //                                     color: AppColors.thirdElement,
          //                                   ),
          //                                   SizedBox(
          //                                     height: 10.h,
          //                                   ),
          //                                   Text(
          //                                     "Tải ảnh lên",
          //                                     style: TextStyle(fontSize: 13.sp),
          //                                   ),
          //                                 ],
          //                               ),
          //                             ),
          //                           ),
          //                   ),
          //                 );
          //               },
          //             ),
          //           ],
          //         ),
          //       ]),
          // );

          Get.defaultDialog(
            title: "Tạo khoá học",
            titleStyle: TextStyle(fontSize: 16.sp),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Tên khoá học",
                    style: TextStyle(fontSize: 13.sp),
                  ),
                  inputTextEdit(
                      controller: controller.coursesController,
                      hintText: "Lớp 12, TOEIC 650,...",
                      fontSize: 13.sp),
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    "Mô tả",
                    style: TextStyle(fontSize: 13.sp),
                  ),
                  SizedBox(
                    height: 80.h,
                    width: 300.w,
                    child: inputTextEditWithBorder(
                      controller: controller.desController,
                      hintText: "Mô tả ngắn gọn về khoá học",
                      maxLines: 2,
                    ),
                  ),
                  Text(
                    "Ảnh khoá học",
                    style: TextStyle(fontSize: 13.sp),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Obx(
                    () {
                      return Center(
                        child: SizedBox(
                          height: 120.h,
                          width: 140.w,
                          child: controller.isUpload.value
                              ? Image.file(controller.image)
                              : Material(
                                  borderRadius: Radii.k6px,
                                  color: AppColors.tabBarElement,
                                  child: InkWell(
                                    borderRadius: Radii.k6px,
                                    highlightColor:
                                        Colors.grey.withOpacity(0.4),
                                    splashColor: Colors.grey.withOpacity(0.5),
                                    onTap: () {
                                      controller.handlePickImage();
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.camera_alt,
                                          size: 21.w,
                                          color: AppColors.thirdElement,
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                        Text(
                                          "Tải ảnh lên",
                                          style: TextStyle(fontSize: 13.sp),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                ],
              ),
            ),
            confirm: btnFlatButtonWidget(
              fontSize: 13.sp,
              onPressed: () {
                controller.handleCreateCourse();
              },
              width: 110.w,
              height: 50.h,
              fontWeight: FontWeight.w500,
              title: "Xác nhận",
            ),
            cancel: btnFlatButtonWidget(
              fontSize: 13.sp,
              onPressed: () {
                controller.handleCloseDialog();
              },
              width: 110.w,
              height: 50.h,
              fontWeight: FontWeight.w500,
              title: "Huỷ bỏ",
              gbColor: AppColors.secondaryElement,
              fontColor: AppColors.primaryText,
            ),
          );
        },
        width: 345.w,
        fontWeight: FontWeight.w600,
        title: "Tạo khoá học",
      );
    }

    Widget _buildRecentCourses() {
      return !controller.isLoadedMyCourse.value
          ? controller.profileController.user.value != []
              ? SizedBox(
                  // height: 220.h,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        ...controller.profileController.user.value!.courses.map(
                          (course) => verCard(
                            height: 300,
                            name: course.name,
                            image: course.image,
                            number: course.number,
                            onPressed: () {
                              controller.handleViewCourse(courseId: course.id);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                )
              : emptyWidget(text: "Chưa có khoá học nào cả 😢")
          : spinKit();
    }

    Widget _buildMyCourses() {
      return !controller.isLoadedMyCourse.value
          ? controller.myCourseList.isNotEmpty
              ? SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ...controller.myCourseList.map((course) => Hero(
                            tag: course.id,
                            child: horCard(
                                name: course.name,
                                image: course.image,
                                number: course.number,
                                onPressed: () {
                                  controller.handleViewCourse(
                                      courseId: course.id);
                                }),
                          ))
                    ],
                  ),
                )
              : emptyWidget(text: "Chưa có khoá học nào cả 😢")
          : spinKit();
    }

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: RefreshIndicator(
          onRefresh: () async {
            await controller.fetchMyCourseList();
            await controller.fetchRecentHistory();
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
              child: Obx(() {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    controller.profileController.user.value!.courses.isNotEmpty
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Truy cập gần đây",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              _buildRecentCourses(),
                            ],
                          )
                        : const SizedBox(),
                    SizedBox(
                      height: 10.h,
                    ),
                    _buildCreateCourse(),
                    SizedBox(
                      height: 10.h,
                    ),
                    Text(
                      "Khoá học của tôi",
                      style: TextStyle(
                          fontSize: 16.sp, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    _buildMyCourses()
                  ],
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}
