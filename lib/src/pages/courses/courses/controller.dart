import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memolary_mobile/src/datas/models/my_course.dart';
import 'package:memolary_mobile/src/datas/networks/course_request.dart';
import 'package:memolary_mobile/src/datas/providers/course_provider.dart';
import 'package:memolary_mobile/src/pages/discover/controller.dart';
import 'package:memolary_mobile/src/widgets/index.dart';
import '../../../datas/models/user.dart';
import '../../../routes/index.dart';
import '../../../utils/index.dart';
import '../../../values/index.dart';
import '../../profile/controller.dart';

class CoursesController extends GetxController {
  final TextEditingController coursesController = TextEditingController();
  final TextEditingController desController = TextEditingController();

  // Create course dialog
  final ImagePicker imagePicker = ImagePicker();
  final Rx<bool> isUpload = false.obs;
  final Rx<String> imageValue = "".obs;
  late File image = File(imageValue.value);
  final Rx<String> imageLink = "".obs;

  final Rx<bool> isLoadedMyCourse = false.obs;

  // My course
  final myCourseList = <MyCourse>[].obs;

  final recentCourses = <MyCourse>[].obs;

  final profileController = Get.find<ProfileController>();

  final discoverController = Get.find<DiscoverController>();

  @override
  void onInit() {
    fetchMyCourseList();
    recentCourses.value = profileController.user.value!.courses;
    super.onInit();
  }

  handleResetDialog() {
    coursesController.clear();
    desController.clear();
    imageValue.value = "";
    isUpload.value = false;
  }

  handlePickImage() async {
    XFile? imageTemp = await imagePicker.pickImage(source: ImageSource.gallery);
    if (imageTemp != null) {
      imageValue.value = imageTemp.path;
      isUpload.value = true;

      image = File(imageValue.value);
      Loading.show();
      final ref = FirebaseStorage.instance
          .ref()
          .child("$storagePath${imageValue.value}${Random().nextInt(99999)}");
      UploadTask uploadTask = ref.putFile(image);
      final snapshot = await uploadTask.whenComplete(() => {});

      final urlDownload = await snapshot.ref
          .getDownloadURL()
          .then((value) => imageLink.value = value);
      Loading.dismiss();

      print("Link: $urlDownload");
      // imageLink.value = urlDownload;
    }
  }

  handleCreateCourse() async {
    FocusManager.instance.primaryFocus?.unfocus();

    String course = coursesController.value.text;
    String description = desController.value.text;

    if (course.isEmpty) {
      toastInfo(msg: 'Vui lòng điền tên khoá học');
      return;
    }

    if (description.isEmpty) {
      toastInfo(msg: 'Vui lòng điền tên mô tả khoá học');
      return;
    }

    if (imageValue.value == "") {
      toastInfo(msg: 'Vui lòng thêm ảnh khoá học');
      return;
    }

    if (imageLink.value == "") {
      toastInfo(
          msg:
              'Hệ thống đã xử lý ảnh của bạn. Vui lòng thử lại trong giây lát...');
      return;
    }
    print(course);

    try {
      Loading.show();
      await CourseProvider().createCourse(
        CourseRequest(
            name: course, image: imageLink.value, description: description),
      );
      Loading.dismiss();
      handleCloseDialog();
      await fetchMyCourseList();
      await fetchRecentHistory();
      await discoverController.fetchAllCourses();
      String courseId = myCourseList[myCourseList.length - 1].id;
      toastInfo(msg: "Tạo khoá học thành công");
      Get.toNamed("/courses/$courseId");
    } catch (e) {
      print(e);
    }
  }

  handleCloseDialog() {
    print("confirm");
    Get.back();
  }

  handleViewCourse({required String courseId}) {
    Get.toNamed("/courses/$courseId");
  }

  fetchMyCourseList() async {
    try {
      isLoadedMyCourse.value = true;
      var myCourses = await CourseProvider().getMyCourses();
      print(myCourses);
      myCourseList.value = myCourses;
      isLoadedMyCourse.value = false;
    } catch (e) {
      print(e);
    }
  }

  fetchRecentHistory() async {
    final userTemp = await profileController.fetchUser();
    recentCourses.value = userTemp.courses;
  }

  @override
  void dispose() {
    coursesController.dispose();
    desController.dispose();
    super.dispose();
  }
}
