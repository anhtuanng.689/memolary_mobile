import 'package:get/get.dart';
import 'package:memolary_mobile/src/pages/courses/courses/controller.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import '../../../datas/models/course.dart';
import '../../../datas/providers/course_provider.dart';
import '../../../routes/index.dart';

class CourseController extends GetxController {
  final course = Rx<Course?>(null);

  final coursesController = Get.find<CoursesController>();

  @override
  void onInit() {
    fetchCourse(courseId: Get.parameters['course']!);
    super.onInit();
  }

  fetchCourse({required String courseId}) async {
    print("hello");
    try {
      course.value = await CourseProvider().getCourse(courseId);
    } catch (e) {
      print(e);
    }
  }

  handleNavViewLesson({required String lessonId}) {
    print(lessonId);
    Get.toNamed("/lessons/$lessonId", arguments: [course.value?.isJoined]);
  }

  handleCreateLesson() {
    Get.toNamed(AppRoutes.createLesson, arguments: [course.value?.id]);
  }

  handleJoinCourse({required String courseId}) async {
    try {
      await CourseProvider().joinCourse(courseId);
      await fetchCourse(courseId: courseId);
      await coursesController.fetchRecentHistory();
      await coursesController.fetchMyCourseList();
    } catch (e) {
      toastInfo(msg: "Tham gia khoá học thất bại");
      print(e);
    }
  }

  handleNavPop() async {
    await coursesController.fetchRecentHistory();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
