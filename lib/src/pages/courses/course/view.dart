import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class CoursePage extends StatelessWidget {
  CoursePage({Key? key}) : super(key: key);

  final controller = Get.find<CourseController>();

  Widget logoCard(
      {required VoidCallback onPressed,
      double width = 355,
      double height = 170,
      required String description,
      required String image,
      required int number,
      required bool isOwner,
      required String owner}) {
    return Card(
      elevation: 3,
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: SizedBox(
        width: width.w,
        height: height.h,
        child: InkWell(
          borderRadius: Radii.k6px,
          onTap: onPressed,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 10.h),
            child: Row(
              children: [
                SizedBox(
                  width: 10.w,
                ),
                Flexible(
                  flex: 4,
                  child: netImageCached(
                    image,
                  ),
                ),
                SizedBox(
                  width: 15.w,
                ),
                Flexible(
                  flex: 6,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              description,
                              style: TextStyle(
                                fontSize: 13.sp,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          const Spacer(),
                          RichText(
                            text: TextSpan(
                              text: 'Bao gồm ',
                              style: TextStyle(
                                fontSize: 12.sp,
                                color: AppColors.primaryText,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: '$number ',
                                  style: TextStyle(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.italic,
                                    color: AppColors.primaryText,
                                  ),
                                ),
                                TextSpan(
                                  text: "từ vựng",
                                  style: TextStyle(
                                    fontSize: 12.sp,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          const Spacer(),
                          RichText(
                            text: TextSpan(
                              text: 'Được tạo bởi ',
                              style: TextStyle(
                                fontSize: 12.sp,
                                color: isOwner?  AppColors.primaryText: Colors.transparent,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: isOwner ? "tôi" : owner,
                                  style: TextStyle(
                                    fontSize: 13.sp,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLogoCourse() {
    return controller.course.value != null
        ? Hero(
            tag: controller.course.value!.id,
            child: logoCard(
                image: controller.course.value!.image,
                description: controller.course.value!.description,
                number: controller.course.value!.number,
                onPressed: () {},
                isOwner: controller.course.value!.isOwner,
                owner: controller.course.value!.owner),
          )
        : spinKit();
  }

  Widget _buildCreateLesson() {
    return btnFlatButtonWidget(
      onPressed: () {
        controller.handleCreateLesson();
      },
      width: 345.w,
      fontWeight: FontWeight.w600,
      title: "Tạo bài học",
    );
  }

  Widget _buildJoinCourse({required bool isJoined}) {
    return isJoined
        ? AbsorbPointer(
            child: btnFlatButtonWidget(
              gbColor: AppColors.fifthElement,
              onPressed: () {},
              width: 345.w,
              fontWeight: FontWeight.w600,
              title: "Đã tham gia khoá học",
            ),
          )
        : btnFlatButtonWidget(
            onPressed: () {
              controller.handleJoinCourse(
                  courseId: controller.course.value!.id);
            },
            width: 345.w,
            fontWeight: FontWeight.w600,
            title: "Tham gia khoá học",
          );
  }

  Widget _buildLesson() {
    return controller.course.value != null
        ? controller.course.value!.lessons.isNotEmpty
            ? GridView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount: controller.course.value!.lessons.length,
                itemBuilder: (context, index) => Card(
                  shape: const RoundedRectangleBorder(
                      borderRadius: Radii.k6pxRadius),
                  child: SizedBox(
                    width: 120.w,
                    height: 180.h,
                    child: InkWell(
                      borderRadius: Radii.k6px,
                      onTap: () {
                        controller.handleNavViewLesson(
                            lessonId:
                                controller.course.value!.lessons[index].id);
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 5.h),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "${index + 1}. ${controller.course.value!.lessons[index].name}",
                              style: TextStyle(
                                fontSize: 15.sp,
                                fontWeight: FontWeight.w600,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            Text(
                              "${controller.course.value!.lessons[index].number} từ",
                              style: TextStyle(
                                fontSize: 13.sp,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 2,
                ),
              )
            : emptyWidget(text: "Chưa có bài học nào cả 😢")
        : spinKit();
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<CourseController>();

    return SafeArea(
      child: Obx(
        () {
          return Scaffold(
            appBar: transparentAppBar(
              leadingCallback: () => controller.handleNavPop(),
              title: controller.course.value != null
                  ? controller.course.value!.name
                  : "",
              actions: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.more_vert,
                    size: 18.w,
                    color: AppColors.thirdElement,
                  ),
                ),
              ],
            ),
            body: RefreshIndicator(
              onRefresh: () async {
                await controller.fetchCourse(
                    courseId: controller.course.value!.id);
              },
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildLogoCourse(),
                      SizedBox(
                        height: 10.h,
                      ),
                      controller.course.value != null
                          ? controller.course.value!.isOwner
                              ? _buildCreateLesson()
                              : _buildJoinCourse(
                                  isJoined: controller.course.value!.isJoined)
                          : spinKit(),
                      SizedBox(
                        height: 10.h,
                      ),
                      _buildLesson(),
                      SizedBox(
                        height: 10.h,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
