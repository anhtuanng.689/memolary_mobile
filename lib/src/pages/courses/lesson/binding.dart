import 'package:get/get.dart';

import 'controller.dart';

class LessonBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LessonController());
  }
}
