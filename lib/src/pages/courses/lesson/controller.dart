import 'package:get/get.dart';

import '../../../datas/models/lesson_learn.dart';
import '../../../datas/providers/lesson_provider.dart';
import '../../../widgets/index.dart';

class LessonController extends GetxController {
  final lesson = Rx<LessonLearn?>(null);
  final learnedWords = 0.obs;
  final isJoined = Get.arguments[0] as bool;

  @override
  void onInit() {
    fetchLesson(lessonId: Get.parameters['lesson']!);
    super.onInit();
  }

  fetchLesson({required String lessonId}) async {
    print(lessonId);
    try {
      lesson.value = await LessonProvider().getLesson(lessonId);
      countLearnedWords();
    } catch (e) {
      print(e);
    }
  }

  countLearnedWords() {
    var count = 0;
    for (var word in lesson.value!.words) {
      if (word.isLearned) {
        count++;
      }
    }
    learnedWords.value = count;
  }

  handleViewWord(int index) {
    Get.toNamed("/lessons/${Get.parameters['lesson']!}/view",
        arguments: [lesson, index]);
  }

  handleLearnWord() {
    if (learnedWords.value == lesson.value!.words.length) {
      toastInfo(msg: 'Bạn đã hết từ để học');
      return;
    }
    Get.toNamed("/lessons/${Get.parameters['lesson']!}/learn");
  }
}
