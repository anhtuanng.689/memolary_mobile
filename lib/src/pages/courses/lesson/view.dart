import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class LessonPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.find<LessonController>();

    Widget _buildWordCount() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Từ vựng",
            style: TextStyle(fontSize: 16.sp),
          ),
          SizedBox(
            width: 5.w,
          ),
          Text(
            "${controller.learnedWords.value}/${controller.lesson.value?.words.length} từ đã học",
            style: TextStyle(fontSize: 13.sp),
          ),
        ],
      );
    }

    _buildWord() {
      return controller.lesson.value?.words.asMap().entries.map(
            (entry) => Column(
              children: [
                Card(
                  // color: entry.value.isLearned
                  //     ? AppColors.fourthElement
                  //     : AppColors.primaryBackground,
                  shape: const RoundedRectangleBorder(
                      borderRadius: Radii.k6pxRadius),
                  child: SizedBox(
                    width: 355.w,
                    height: 80.h,
                    child: InkWell(
                      borderRadius: Radii.k6px,
                      onTap: () {
                        controller.handleViewWord(entry.key);
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.w, vertical: 10.h),
                        child: Row(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  entry.value.word,
                                  style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Text(
                                  entry.value.wordMeaning,
                                  style: TextStyle(
                                    fontSize: 13.sp,
                                  ),
                                ),
                              ],
                            ),
                            const Spacer(),
                            entry.value.isLearned
                                ? Image.asset(
                                    "assets/images/check-mark.png",
                                    height: 25.w,
                                    fit: BoxFit.cover,
                                  )
                                : const SizedBox(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
    }

    Widget _buildWordList() {
      return Column(
        children: [...?_buildWord()],
      );
    }

    return SafeArea(
      child: Obx(() {
        return controller.lesson.value != null
            ? controller.isJoined
                ? Scaffold(
                    appBar: transparentAppBar(
                      title: controller.lesson.value!.name,
                      actions: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.more_vert,
                            size: 18.w,
                            color: AppColors.thirdElement,
                          ),
                        ),
                      ],
                    ),
                    floatingActionButton: btnFlatButtonWidget(
                      onPressed: () {
                        controller.handleLearnWord();
                      },
                      width: 325.w,
                      fontWeight: FontWeight.w600,
                      title: "Học từ vựng",
                    ),
                    floatingActionButtonLocation:
                        FloatingActionButtonLocation.centerFloat,
                    body: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 10.h),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _buildWordCount(),
                            SizedBox(
                              height: 10.h,
                            ),
                            _buildWordList(),
                            SizedBox(
                              height: 100.h,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : Scaffold(
                    appBar: transparentAppBar(
                      title: controller.lesson.value!.name,
                      actions: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.more_vert,
                            size: 18.w,
                            color: AppColors.thirdElement,
                          ),
                        ),
                      ],
                    ),
                    body: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 10.h),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _buildWordCount(),
                            SizedBox(
                              height: 10.h,
                            ),
                            _buildWordList(),
                            SizedBox(
                              height: 100.h,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
            : spinKit();
      }),
    );
  }
}
