import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/pages/courses/learn/widgets/flash_card_lesson.dart';

import '../../../widgets/index.dart';
import 'index.dart';

class LearnPage extends StatelessWidget {
  LearnPage({Key? key}) : super(key: key);

  final controller = Get.find<LearnController>();

  Widget _buildLesson(int index, int lessonMode) {
    if (controller.learnQueue.isNotEmpty) {
      if (controller.lessonMode.value == 0) {
        return Center(
          child: buildFlashCardLesson(
            word: controller.learnQueue[index],
            flipCardController: controller.flipCardController[index],
            onPress: () => controller.speak(controller.learnQueue[index].word),
          ),
        );
      } else if (controller.lessonMode.value == 1) {
        return buildChooseAnswerLesson(
            words: controller.randomList,
            controller: controller,
            word: controller.learnQueue[index]);
      } else {
        return buildSpellWordLesson(
            inputController: controller.learnInputController,
            onPressed: () {
              controller.speak(controller.learnQueue[index].word);
            });
      }
    } else {
      return spinKit();
    }
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<LearnController>();

    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Obx(
          () {
            return Scaffold(
              appBar: indicatorAppBar(
                percent: controller.learnQueue.isNotEmpty
                    ? controller.indicator.value /
                        (controller.learnQueue.length * 2)
                    : 0,
              ),
              floatingActionButton: btnFlatButtonWidget(
                onPressed: () {
                  controller.checkResult(context);
                },
                width: 325.w,
                fontWeight: FontWeight.w600,
                title: "Kiểm tra",
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              body: SingleChildScrollView(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                physics: const NeverScrollableScrollPhysics(),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                  child: Obx(
                    () {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _buildLesson(controller.wordIndex.value,
                              controller.lessonMode.value),
                          SizedBox(
                            height: 10.h,
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
