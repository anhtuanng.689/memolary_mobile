import 'package:get/get.dart';

import 'controller.dart';

class LearnBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LearnController());
  }
}
