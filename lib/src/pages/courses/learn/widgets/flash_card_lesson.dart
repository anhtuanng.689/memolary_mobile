import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../datas/models/word.dart';
import '../../../../values/index.dart';
import '../../../../widgets/index.dart';

Widget _buildBaseFlashCard({required Widget widget}) {
  return Card(
    elevation: 5,
    shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
      child: widget,
    ),
  );
}

Widget _buildFrontFlashCard(Word word, VoidCallback onPress) {
  return _buildBaseFlashCard(
    widget: Stack(
      fit: StackFit.expand,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15.h,
            ),
            Expanded(
              child: netImageCached(
                word.image,
                fit: BoxFit.fitWidth,
              ),
            ),
            SizedBox(
              height: 15.h,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  word.sentence,
                  style: TextStyle(fontSize: 13.sp),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Text(
                  word.sentenceMeaning,
                  style: TextStyle(fontSize: 13.sp),
                ),
              ],
            )
          ],
        ),
        Positioned(
          top: 0,
          right: 0,
          child: RawMaterialButton(
            constraints: BoxConstraints(minWidth: 40.w, minHeight: 50.h),
            onPressed: onPress,
            elevation: 5,
            fillColor: AppColors.primaryElement,
            child: Icon(
              Icons.volume_up,
              size: 26.w,
              color: AppColors.primaryBackground,
            ),
            shape: const CircleBorder(),
          ),
        ),
      ],
    ),
  );
}

Widget _buildBackFlashCard(Word word) {
  return _buildBaseFlashCard(
    widget: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          word.word,
          style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
        ),
        word.pronounce != " "
            ? Column(
                children: [
                  SizedBox(
                    height: 20.h,
                  ),
                  Text(
                    "/${word.pronounce}/",
                    style: GoogleFonts.arimo(
                      textStyle: TextStyle(
                          fontSize: 16.sp, fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                ],
              )
            : SizedBox(
                height: 20.h,
              ),
        Text(
          word.wordMeaning,
          style: TextStyle(
            fontSize: 16.sp,
          ),
        ),
      ],
    ),
  );
}

Widget _buildFlashCard(
    Word word, FlipCardController flipCardController, VoidCallback onPress) {
  return SizedBox(
    width: 350.w,
    height: 590.h,
    child: FlipCard(
      controller: flipCardController,
      fill: Fill
          .fillBack, // Fill the back side of the card to make in the same size as the front.
      direction: FlipDirection.HORIZONTAL, // default
      front: _buildFrontFlashCard(word, onPress),
      back: _buildBackFlashCard(word),
    ),
  );
}

Widget buildFlashCardLesson(
    {required Word word,
    required FlipCardController flipCardController,
    required VoidCallback onPress}) {
  return Column(
    children: [
      SizedBox(
        height: 20.h,
      ),
      _buildFlashCard(word, flipCardController, onPress),
    ],
  );
}
