import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../datas/models/word.dart';
import '../../../../values/index.dart';
import '../../../../widgets/index.dart';
import '../index.dart';

Widget _buildChooseAnswerCard({
  required VoidCallback onPressed,
  required String choice,
  required bool isToggle,
  String? image,
}) {
  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(
      borderRadius: Radii.k6pxRadius,
      side: isToggle
          ? BorderSide(color: Colors.blue, width: 3.w)
          : BorderSide.none,
    ),
    child: InkWell(
      borderRadius: Radii.k6px,
      onTap: onPressed,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            image != null
                ? Expanded(
                    child: netImageCached(
                      image,
                    ),
                  )
                : const SizedBox(),
            SizedBox(
              height: 3.h,
            ),
            Text(
              choice,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget _buildGridChooseAnswer(List<Word> words, controller) {
  return GridView.count(
    physics: const NeverScrollableScrollPhysics(),
    crossAxisCount: 2,
    mainAxisSpacing: 15.h,
    crossAxisSpacing: 5.w,
    shrinkWrap: true,
    children: [
      _buildChooseAnswerCard(
        choice: words[0].word,
        image: words[0].image,
        isToggle: controller.isToggleListChooseAnswer[0],
        onPressed: () {
          controller.toggleChooseAnswer(0);
        },
      ),
      _buildChooseAnswerCard(
        choice: words[1].word,
        image: words[1].image,
        isToggle: controller.isToggleListChooseAnswer[1],
        onPressed: () {
          controller.toggleChooseAnswer(1);
        },
      ),
      _buildChooseAnswerCard(
        choice: words[2].word,
        image: words[2].image,
        isToggle: controller.isToggleListChooseAnswer[2],
        onPressed: () {
          controller.toggleChooseAnswer(2);
        },
      ),
      _buildChooseAnswerCard(
        choice: words[3].word,
        image: words[3].image,
        isToggle: controller.isToggleListChooseAnswer[3],
        onPressed: () {
          controller.toggleChooseAnswer(3);
        },
      ),
    ],
  );
}

Widget buildChooseAnswerLesson(
    {required List<Word> words,
    required LearnController controller,
    required Word word}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Từ nào dưới đây có nghĩa là \"${word.wordMeaning}\"?",
        style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      _buildGridChooseAnswer(words, controller)
    ],
  );
}
