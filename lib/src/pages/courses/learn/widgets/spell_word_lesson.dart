import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../values/index.dart';
import '../../../../widgets/index.dart';

Widget _buildSoundButton(VoidCallback onPressed) {
  return Center(
    child: RawMaterialButton(
      constraints: BoxConstraints(minWidth: 80.w, minHeight: 80.h),
      onPressed: onPressed,
      elevation: 5,
      fillColor: AppColors.primaryElement,
      child: Icon(
        Icons.volume_up,
        size: 40.w,
        color: AppColors.primaryBackground,
      ),
      shape: const CircleBorder(),
    ),
  );
}

Widget _buildSpellCard(TextEditingController inputController) {
  return inputCardEdit(
    controller: inputController,
    fontSize: 18,
  );
}

Widget buildSpellWordLesson(
    {required TextEditingController inputController,
    required VoidCallback onPressed}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Nhập lại nội dung vừa nghe",
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 30.h,
      ),
      _buildSoundButton(onPressed),
      SizedBox(
        height: 30.h,
      ),
      _buildSpellCard(inputController),
    ],
  );
}
