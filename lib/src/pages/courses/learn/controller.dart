import 'dart:io';
import 'dart:math';

import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:memolary_mobile/src/datas/models/lesson_learn.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';
import 'package:memolary_mobile/src/pages/courses/lesson/controller.dart';
import 'package:memolary_mobile/src/pages/review/review/controller.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../datas/networks/learned_word_request.dart';
import '../../../datas/providers/learn_provider.dart';
import '../../../datas/providers/lesson_provider.dart';
import '../../../utils/index.dart';
import '../../../values/index.dart';
import '../../../widgets/index.dart';

class LearnController extends GetxController {
  final learnQueue = <Word>[].obs;

  Random random = Random();

  final lessonLearn = Rx<LessonLearn?>(null);

  final randomList = <Word>[].obs;

  final RxInt wordIndex = 0.obs;

  final RxInt lessonMode = 0.obs;

  final RxInt indicator = 0.obs;

  final List<bool> isToggleListChooseAnswer = [false, false, false, false].obs;

  final FlutterTts flutterTts = FlutterTts();

  final TextEditingController learnInputController = TextEditingController();

  final List<FlipCardController> flipCardController =
      [FlipCardController()].obs;

  final RxBool isShowMeaning = false.obs;

  final LessonController lessonController = Get.find();

  final ReviewController reviewController = Get.find();

  @override
  void onInit() {
    fetchWordsToLearn();
    setupTts();

    ever(lessonMode, (value) {
      if (lessonMode.value == 0 || lessonMode.value == 2) {
        speak(learnQueue[wordIndex.value].word);
      } else if (lessonMode.value == 1) {
        shuffleRandomList();
      }
    });
    super.onInit();
  }

  shuffleRandomList() {
    randomList.clear();
    randomList.add(learnQueue[wordIndex.value]);
    while (randomList.length < 4) {
      int rand = random.nextInt(learnQueue.length);
      if (!randomList.contains(learnQueue[rand])) {
        randomList.add(learnQueue[rand]);
      }
    }
    randomList.shuffle();
  }

  setupTts() async {
    await flutterTts.setLanguage("en-US");
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(0.8);
    speak(learnQueue[0].word);
  }

  prepareLearn(List<Word> words) {
    // print("lesson Learn: $words");
    for (var element in words) {
      learnQueue.add(element);
      flipCardController.add(FlipCardController());
    }
    // print("lesson queue: $learnQueue");
  }

  fetchWordsToLearn() async {
    // print(Get.parameters['lesson']!);
    try {
      lessonLearn.value = await LearnProvider()
          .getWordsToLearn(Get.parameters['lesson']!)
          .then((value) => prepareLearn(value.words));
    } catch (e) {
      print(e);
    }
  }

  speak(String text) async {
    await flutterTts.speak(text);
  }

  checkResult(BuildContext context) {
    var result = false;
    if (lessonMode.value == 1) {
      int index = isToggleListChooseAnswer.indexOf(true);
      if (randomList[index].word == learnQueue[wordIndex.value].word) {
        result = true;
      }
    } else if (lessonMode.value == 2) {
      if (learnInputController.value.text == learnQueue[wordIndex.value].word) {
        result = true;
      }
    }
    if (result) {
      indicator.value++;
    }
    showResult(context, result);
  }

  toggleNext(bool result) {
    // print((wordIndex.value + 1) / learnQueue.length);
    FocusManager.instance.primaryFocus?.unfocus();
    print(result);

    if (lessonMode.value == 0) {
      lessonMode.value++;
    } else if (lessonMode.value == 1) {
      if (!result) {
        // print("Ok");
        resetMultipleChoice();
        return;
      }
      lessonMode.value++;
      resetMultipleChoice();
    } else if (lessonMode.value == 2) {
      if (!result) {
        speak(learnQueue[wordIndex.value].word);
        resetLearnInput();
        return;
      }
      if (wordIndex.value == learnQueue.length - 1) {
        List<String> words = [];
        for (var word in learnQueue) {
          words.add(word.id);
        }
        print(words);
        updateLearnedWords(words);
        finishLearn();
        return;
      }
      resetLearnInput();
      wordIndex.value++;
      lessonMode.value = 0;
    }
    // print(lessonMode.value);
  }

  toggleChooseAnswer(int index) {
    resetMultipleChoice();
    isToggleListChooseAnswer[index] = true;
  }

  resetLearnInput() {
    learnInputController.clear();
  }

  resetMultipleChoice() {
    for (int i = 0; i < isToggleListChooseAnswer.length; i++) {
      isToggleListChooseAnswer[i] = false;
    }
  }

  showResult(BuildContext context, bool result) {
    if (lessonMode.value != 0) {
      showMaterialModalBottomSheet(
        isDismissible: false,
        context: context,
        builder: (context) => Obx(() {
          return SizedBox(
            height: 400.h,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Image.asset(
                      result
                          ? "assets/images/right.png"
                          : "assets/images/wrong.png",
                      scale: 5,
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.w),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  learnQueue[wordIndex.value].word,
                                  style: TextStyle(
                                      fontSize: 20.sp,
                                      fontWeight: FontWeight.w600),
                                ),
                                learnQueue[wordIndex.value].pronounce != " "
                                    ? Column(
                                        children: [
                                          SizedBox(
                                            height: 5.h,
                                          ),
                                          Text(
                                            "/${learnQueue[wordIndex.value].pronounce}/",
                                            style: GoogleFonts.arimo(
                                              textStyle: TextStyle(
                                                fontSize: 13.sp,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5.h,
                                          ),
                                        ],
                                      )
                                    : SizedBox(
                                        height: 5.h,
                                      ),
                                Text(
                                  learnQueue[wordIndex.value].wordMeaning,
                                  style: TextStyle(fontSize: 13.sp),
                                ),
                                SizedBox(
                                  height: 5.h,
                                ),
                                Text(
                                  learnQueue[wordIndex.value].sentence,
                                  style: TextStyle(fontSize: 13.sp),
                                ),
                                SizedBox(
                                  height: 5.h,
                                ),
                                Text(
                                  learnQueue[wordIndex.value].sentenceMeaning,
                                  style: TextStyle(
                                      fontSize: 13.sp,
                                      color: isShowMeaning.value
                                          ? AppColors.primaryText
                                          : Colors.transparent),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                RawMaterialButton(
                                  constraints: BoxConstraints(
                                      minWidth: 40.w, minHeight: 50.h),
                                  onPressed: () {
                                    speak(learnQueue[wordIndex.value].word);
                                  },
                                  elevation: 5,
                                  fillColor: AppColors.primaryElement,
                                  child: Icon(
                                    Icons.volume_up,
                                    size: 26.w,
                                    color: AppColors.primaryBackground,
                                  ),
                                  shape: const CircleBorder(),
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                RawMaterialButton(
                                  constraints: BoxConstraints(
                                      minWidth: 40.w, minHeight: 50.h),
                                  onPressed: () {
                                    isShowMeaning.value = !isShowMeaning.value;
                                  },
                                  elevation: 5,
                                  fillColor: AppColors.primaryElement,
                                  child: Icon(
                                    Icons.translate_outlined,
                                    size: 26.w,
                                    color: AppColors.primaryBackground,
                                  ),
                                  shape: const CircleBorder(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  btnFlatButtonWidget(
                    onPressed: () {
                      Get.back();
                      sleep(const Duration(milliseconds: 300));
                      toggleNext(result);
                      isShowMeaning.value = false;
                    },
                    width: 325.w,
                    fontWeight: FontWeight.w600,
                    title: "Tiếp tục",
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                ],
              ),
            ),
          );
        }),
      );
    } else {
      toggleNext(result);
    }
  }

  updateLearnedWords(List<String> words) async {
    try {
      Loading.show();
      await LessonProvider().submitLearnedWords(
        Get.parameters['lesson']!,
        LearnedWordRequest(words: words),
      );
      await lessonController.fetchLesson(lessonId: Get.parameters['lesson']!);
      Loading.dismiss();
    } catch (e) {
      print(e);
    }
  }

  finishLearn() async {
    await reviewController.fetchAllWordsReview();
    await reviewController.fetchReview();
    Get.back();
  }
}
