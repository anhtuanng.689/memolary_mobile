import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:memolary_mobile/src/pages/courses/view_word/controller.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';

class ViewWordPage extends StatelessWidget {
  ViewWordPage({Key? key}) : super(key: key);

  final controller = Get.find<ViewWordController>();

  Widget _buildBaseFlashCard({required Widget widget}) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 30.h),
        child: widget,
      ),
    );
  }

  Widget _buildFrontFlashCard(
      {required String word,
      required String pronounce,
      required String sentence,
      required String sentenceMeaning,
      required String image}) {
    return _buildBaseFlashCard(
      widget: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: RawMaterialButton(
              constraints: BoxConstraints(minWidth: 40.w, minHeight: 50.h),
              onPressed: () {
                controller.speak(word);
              },
              elevation: 5,
              fillColor: AppColors.primaryElement,
              child: Icon(
                Icons.volume_up,
                size: 26.w,
                color: AppColors.primaryBackground,
              ),
              shape: const CircleBorder(),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Text(
                  word,
                  style:
                      TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              Column(
                children: [
                  Center(
                    child: Text(
                      "/$pronounce/",
                      style: GoogleFonts.arimo(
                        textStyle: TextStyle(
                            color: pronounce != " "
                                ? AppColors.primaryText
                                : Colors.transparent,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15.h,
              ),
              Expanded(
                child: netImageCached(
                  image,
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(
                height: 15.h,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Ví dụ",
                    style:
                        TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    sentence,
                    style: TextStyle(fontSize: 13.sp),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    sentenceMeaning,
                    style: TextStyle(fontSize: 13.sp),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBackFlashCard({required String wordMeaning}) {
    return _buildBaseFlashCard(
      widget: Center(
        child: Text(
          wordMeaning,
          style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
        ),
      ),
    );
  }

  // Widget _buildFlashCard() {
  //   return SizedBox(
  //     width: 330.w,
  //     height: 600.h,
  //     child: FlipCard(
  //       fill: Fill
  //           .fillBack, // Fill the back side of the card to make in the same size as the front.
  //       direction: FlipDirection.HORIZONTAL, // default
  //       front: _buildFrontFlashCard(
  //           word: '', pronounce: '', sentence: '', sentenceMeaning: ''),
  //       back: _buildBackFlashCard(wordMeaning: ''),
  //     ),
  //   );
  // }

  Widget _buildFlashCardPageView() {
    return PageView(
      controller: controller.pageController,
      onPageChanged: (index) {
        controller.updateCurrentPage(index);
      },
      children: [
        ...controller.lesson.value!.words.map(
          (word) => SizedBox(
            width: 330.w,
            height: 600.h,
            child: FlipCard(
              fill: Fill.fillBack,
              // Fill the back side of the card to make in the same size as the front.
              direction: FlipDirection.HORIZONTAL,
              // default
              front: _buildFrontFlashCard(
                word: word.word,
                pronounce: word.pronounce,
                sentence: word.sentence,
                sentenceMeaning: word.sentenceMeaning,
                image: word.image,
              ),
              back: _buildBackFlashCard(wordMeaning: word.wordMeaning),
            ),
          ),
        ),
      ],
    );
    // return PageView.builder(
    //   controller: controller.pageController,
    //   itemBuilder: (context, position) {
    //     return _buildFlashCard();
    //   },
    //   itemCount: 10,
    // );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ViewWordController>();

    return SafeArea(
      child: Obx(() {
        return controller.lesson.value != null
            ? Scaffold(
                appBar: transparentAppBar(
                  title:
                      "${controller.pageIndex.value + 1}/${controller.lesson.value!.words.length}",
                ),
                body: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 40.h),
                  child: _buildFlashCardPageView(),
                ),
              )
            : spinKit();
      }),
    );
  }
}
