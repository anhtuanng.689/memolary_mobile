import 'package:flutter/cupertino.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';

import '../../../datas/models/lesson_learn.dart';
import 'index.dart';

class ViewWordController extends GetxController {
  final lesson = Get.arguments[0] as Rx<LessonLearn?>;
  final int idx = Get.arguments[1] as int;

  final Rx<int> pageIndex = 0.obs;

  late final PageController pageController;

  final FlutterTts flutterTts = FlutterTts();

  @override
  void onInit() {
    pageController = PageController(
      initialPage: idx,
    );
    updateCurrentPage(idx);
    super.onInit();
  }

  setupTts() async {
    await flutterTts.setLanguage("en-US");
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(0.8);
  }

  speak(String text) async {
    await flutterTts.speak(text);
  }

  updateCurrentPage(int index) {
    pageIndex.value = index;
  }
}
