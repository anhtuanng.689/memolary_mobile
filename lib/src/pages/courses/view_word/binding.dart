import 'package:get/get.dart';

import 'controller.dart';

class ViewWordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ViewWordController());
  }
}
