import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class SignUpPage extends GetView<SignUpController> {
  const SignUpPage({Key? key}) : super(key: key);

  Widget _buildTitle() {
    return Center(
      child: Text(
        "Đăng ký",
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 18.sp,
        ),
      ),
    );
  }

  Widget _buildInputForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        inputTextEdit(
          controller: controller.nameController,
          keyboardType: TextInputType.text,
          hintText: "Tên hiển thị",
          marginTop: 0,
        ),
        inputTextEdit(
          controller: controller.emailController,
          keyboardType: TextInputType.emailAddress,
          hintText: "Email",
        ),
        inputTextEdit(
          controller: controller.passController,
          keyboardType: TextInputType.visiblePassword,
          hintText: "Mật khẩu",
          isPassword: true,
        ),
        SizedBox(
          height: 25.h,
        ),
        btnFlatButtonWidget(
          onPressed: controller.handleSignUp,
          width: 295.w,
          fontWeight: FontWeight.w600,
          title: "Tạo tài khoản",
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: TextButton(
            onPressed: controller.handleForgotPassword,
            child: Text(
              "Quên mật khẩu?",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.secondaryElementText,
                fontWeight: FontWeight.w400,
                fontSize: 16.sp,
                height: 1,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildThirdPartyLogin() {
    return SizedBox(
      child: Center(
        child: Column(
          children: <Widget>[
            Text(
              "Hoặc đăng nhập với",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.primaryText,
                fontWeight: FontWeight.w400,
                fontSize: 16.sp,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.h),
              child: btnFlatButtonBorderOnlyWidget(
                onPressed: () {},
                width: 88,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildHaveAccountButton() {
    return Center(
      child: btnFlatButtonWidget(
        onPressed: controller.handleNavPop,
        width: 295.w,
        gbColor: AppColors.secondaryElement,
        fontColor: AppColors.primaryText,
        title: "Tôi đã có tài khoản",
        fontWeight: FontWeight.w500,
        fontSize: 16,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildTitle(),
              _buildInputForm(),
              _buildThirdPartyLogin(),
              _buildHaveAccountButton(),
            ],
          ),
        ),
      ),
    );
  }
}
