import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/networks/register_request.dart';

import '../../../datas/providers/auth_provider.dart';
import '../../../datas/providers/network_exception.dart';
import '../../../utils/index.dart';
import '../../../widgets/index.dart';

class SignUpController extends GetxController {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  handleNavPop() {
    Get.back();
  }

  handleTip() {
    toastInfo(msg: 'Tính năng đang phát triển...');
  }

  handleForgotPassword() {
    toastInfo(msg: 'Tính năng đang phát triển...');
  }

  handleSignUp() async {
    FocusManager.instance.primaryFocus?.unfocus();

    String email = emailController.value.text;
    String password = passController.value.text;
    String name = nameController.value.text;

    if (!checkStringLength(name, 1)) {
      toastInfo(msg: 'Tên hiển thị quá ngắn');
      return;
    }
    if (!isEmail(email)) {
      toastInfo(msg: 'Email không hợp lệ');
      return;
    }
    if (!checkStringLength(password, 6)) {
      toastInfo(msg: 'Mật khẩu ít nhất 6 ký tự');
      return;
    }
    print(name);
    print(email);
    print(password);

    try {
      await AuthProvider().register(
        RegisterRequest(email: email, password: password, name: name),
      );
      toastInfo(msg: 'Đăng ký thành công');
      Get.back();
    } on UnauthorisedException catch (e) {
      print(e);
    } catch (e) {
      print(e);
      toastInfo(msg: 'Đã có lỗi xảy ra...');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }
}
