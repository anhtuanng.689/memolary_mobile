import 'package:get/get.dart';
import 'package:memolary_mobile/src/routes/app_routes.dart';

import '../../../values/storage.dart';

class WelcomeController extends GetxController {
  handleNavSignIn() {
    Storage().saveFirstOpen();
    Get.offAllNamed(AppRoutes.signIn);
  }
}
