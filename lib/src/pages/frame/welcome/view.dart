import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../../../values/index.dart';
import 'controller.dart';

class WelcomePage extends GetView<WelcomeController> {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.find<WelcomeController>();

    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.only(top: 0.2.sh),
          child: IntroductionScreen(
            pages: [
              PageViewModel(
                title: "Khám phá",
                body: "Kho tàng từ vựng rộng lớn",
                image: SvgPicture.asset(
                  "assets/images/discover.svg",
                  height: 0.4.sh,
                ),
              ),
              PageViewModel(
                title: "Luyện tập",
                body: "Từ vựng mọi lúc mọi nơi",
                image: Center(
                  child: SvgPicture.asset(
                    "assets/images/learn.svg",
                  ),
                ),
              ),
              PageViewModel(
                title: "Nhắc nhở",
                body: "Thời điểm ôn luyện hàng ngày",
                image: Center(
                  child: SvgPicture.asset(
                    "assets/images/practice.svg",
                  ),
                ),
              ),
            ],
            onDone: () {
              controller.handleNavSignIn();
            },
            onSkip: () {
              controller.handleNavSignIn();
            },
            showBackButton: false,
            showSkipButton: true,
            skip: Text(
              "Bỏ qua",
              style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
            ),
            next: Icon(
              Icons.navigate_next,
              size: 18.w,
              color: AppColors.thirdElement,
            ),
            done: Text(
              "Bắt đầu",
              style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
            ),
            dotsDecorator: DotsDecorator(
              size: Size.square(10.w),
              activeSize: Size(20.w, 10.w),
              // activeColor: theme.accentColor,
              // color: Colors.black26,
              spacing: EdgeInsets.symmetric(horizontal: 3.w),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
