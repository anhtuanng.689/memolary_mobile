import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/networks/login_request.dart';

import '../../../datas/networks/user_request.dart';
import '../../../datas/providers/auth_provider.dart';
import '../../../datas/providers/network_exception.dart';
import '../../../datas/providers/user_provider.dart';
import '../../../routes/index.dart';
import '../../../utils/index.dart';
import '../../../values/index.dart';
import '../../../widgets/index.dart';

class SignInController extends GetxController {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final accessToken = "".obs;
  // final MyRepository repository;
  // SignInController({@required this.repository}) : assert(repository != null);

  handleNavSignUp() {
    Get.toNamed(AppRoutes.signUp);
  }

  handleForgotPassword() {
    toastInfo(msg: 'Tính năng đang phát triển');
  }

  handleSignIn() async {
    FocusManager.instance.primaryFocus?.unfocus();

    String email = emailController.value.text;
    String password = passController.value.text;
    if (!isEmail(email)) {
      toastInfo(msg: 'Email không hợp lệ');
      return;
    }
    if (!checkStringLength(password, 6)) {
      toastInfo(msg: 'Mật khẩu ít nhất 6 ký tự');
      return;
    }
    print(email);
    print(password);

    try {
      accessToken.value = await AuthProvider()
          .signIn(LoginRequest(email: email, password: password));
      Storage().saveAccessToken(accessToken.value);
      FirebaseMessaging.instance
          .getToken(vapidKey: 'AIzaSyBLdxdp12jdGWTwExxeXo4dDnrErM8AxJU')
          .then((fcmToken) async {
        print('FCM Token: $fcmToken');

        if (Storage().getIsLogined() == null) {
          await UserProvider().updateUser(UserRequest(fcmToken: fcmToken!));
          Storage().saveIsLogined();
        }
      });
    } on UnauthorisedException catch (e) {
      print(e);
    } catch (e) {
      toastInfo(msg: 'Mật khẩu không đúng');
      print(e);
    }
    if (Storage().isAccessToken(accessToken.value)) {
      Get.offAndToNamed(AppRoutes.dashboard);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }
}
