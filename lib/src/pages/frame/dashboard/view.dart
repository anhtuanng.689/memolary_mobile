import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

import '../../courses/courses/index.dart';
import '../../discover/index.dart';
import '../../profile/index.dart';
import '../../review/review/index.dart';
import 'controller.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<DashboardController>();

    final tabs = [ReviewPage(), CoursesPage(), DiscoverPage(), ProfilePage()];

    return SafeArea(
      child: Obx(() {
        return Scaffold(
          body: tabs[controller.currentIndex.value],
          bottomNavigationBar: SalomonBottomBar(
            margin: EdgeInsets.all(10.h),
            currentIndex: controller.currentIndex.value,
            onTap: (i) {
              print(i);
              controller.changeTabIndex(i);
            },
            items: [
              SalomonBottomBarItem(
                icon: Image.asset(
                  "assets/images/review.png",
                  height: 25.w,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  "Ôn tập",
                  style:
                      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                ),
                selectedColor: Colors.pinkAccent,
              ),
              SalomonBottomBarItem(
                icon: Image.asset(
                  "assets/images/learn.png",
                  height: 25.w,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  "Khoá học",
                  style:
                      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                ),
                selectedColor: Colors.orangeAccent,
              ),
              SalomonBottomBarItem(
                icon: Image.asset(
                  "assets/images/search.png",
                  height: 25.w,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  "Tìm kiếm",
                  style:
                      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                ),
                selectedColor: Colors.lightBlueAccent,
              ),
              SalomonBottomBarItem(
                icon: Image.asset(
                  "assets/images/user.png",
                  height: 25.w,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  "Cá nhân",
                  style:
                      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
                ),
                selectedColor: Colors.deepPurpleAccent,
              ),
            ],
          ),
        );
      }),
    );
  }
}
