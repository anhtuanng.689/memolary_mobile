import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../widgets/index.dart';
import 'controller.dart';

class DiscoverPage extends StatelessWidget {
  const DiscoverPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<DiscoverController>();

    Widget _buildSapo() {
      return Text(
        "Bạn muốn học gì hôm nay?",
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 16.sp,
        ),
      );
    }

    Widget _buildSearch() {
      return SizedBox(
        child: inputTextEdit(
          controller: controller.searchController,
          onChanged: (text) {
            controller.search(text);
          },
          hintText: "Tìm kiếm",
          fontSize: 13,
        ),
      );
    }

    Widget _buildPopular() {
      return SizedBox(
        height: 250.h,
        child: Swiper(
          autoplay: true,
          itemBuilder: (BuildContext context, int index) {
            return verCard(
              name: controller.popularCourses[index].name,
              image: controller.popularCourses[index].image,
              number: controller.popularCourses[index].number,
              onPressed: () {
                controller.handleViewCourse(
                    courseId: controller.popularCourses[index].id);
              },
            );
          },
          itemCount: 5,
          viewportFraction: 0.8,
          scale: 0.9,
        ),
      );
    }

    Widget _buildAllCourses() {
      return !controller.isLoadedCourses.value
          ? controller.courses.isNotEmpty
              ? SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ...controller.courses.map(
                        (course) => Hero(
                          tag: course.id,
                          child: horCard(
                            name: course.name,
                            image: course.image,
                            number: course.number,
                            onPressed: () {
                              controller.handleViewCourse(courseId: course.id);
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                )
              : emptyWidget(text: "Chưa có khoá học nào cả 😢")
          : spinKit();
    }

    Widget _buildSearchCourses() {
      return
          // !controller.isSearch.value
          controller.searchCourses.isNotEmpty
              ? SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ...controller.searchCourses.map(
                        (course) => Hero(
                          tag: course.id,
                          child: horCard(
                            name: course.name,
                            image: course.image,
                            number: course.number,
                            onPressed: () {
                              controller.handleViewCourse(courseId: course.id);
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                )
              : emptyWidget(text: "Không tìm thấy khoá học 😢")
          // : spinKit()
          ;
    }

    return SafeArea(
      child: Obx(() {
        return Scaffold(
          body: RefreshIndicator(
            onRefresh: () async {
              await controller.fetchAllCourses();
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildSapo(),
                    _buildSearch(),
                    controller.isSearch.value
                        ? _buildSearchCourses()
                        : Column(
                            children: [
                              SizedBox(
                                height: 10.h,
                              ),
                              !controller.isLoadedPopularCourses.value &&
                                      controller.popularCourses.length >= 5
                                  ? Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Phổ biến",
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                        _buildPopular(),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                      ],
                                    )
                                  : const SizedBox(),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Tất cả khoá học",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    "Xem thêm",
                                    style: TextStyle(
                                        fontSize: 13.sp,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              _buildAllCourses(),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
