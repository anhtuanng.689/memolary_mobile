import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/models/my_course.dart';

import '../../datas/providers/course_provider.dart';
import '../../utils/index.dart';

class DiscoverController extends GetxController {
  final TextEditingController searchController = TextEditingController();
  final debounce = Debounce(const Duration(milliseconds: 500));

  final courses = <MyCourse>[].obs;
  final isLoadedCourses = false.obs;

  final popularCourses = <MyCourse>[].obs;
  final isLoadedPopularCourses = false.obs;

  final searchCourses = <MyCourse>[].obs;
  final isSearch = false.obs;

  final query = "".obs;

  @override
  void onInit() {
    fetchAllCourses();
    fetchPopularCourses();
    super.onInit();
  }

  handleViewCourse({required String courseId}) {
    FocusManager.instance.primaryFocus?.unfocus();
    Get.toNamed("/courses/$courseId");
  }

  search(String query) async {
    debounce.call(() async {
      try {
        if (query == "") {
          searchCourses.value = [];
          isSearch.value = false;
        } else {
          var coursesTemp = await CourseProvider().getAllCourses(query: query);
          print(coursesTemp);
          isSearch.value = true;
          searchCourses.value = coursesTemp;
        }
      } catch (e) {
        isSearch.value = false;
        print(e);
      }
    });
  }

  fetchPopularCourses() async {
    try {
      isLoadedPopularCourses.value = true;
      var coursesTemp = await CourseProvider().getPopularCourses();
      print(coursesTemp);
      popularCourses.value = coursesTemp;
      isLoadedPopularCourses.value = false;
    } catch (e) {
      print(e);
    }
  }

  fetchAllCourses() async {
    try {
      isLoadedCourses.value = true;
      var coursesTemp = await CourseProvider().getAllCourses();
      print(coursesTemp);
      courses.value = coursesTemp;
      isLoadedCourses.value = false;
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }
}
