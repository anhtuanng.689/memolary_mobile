import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'controller.dart';

class HistoryPage extends StatelessWidget {
  HistoryPage({Key? key}) : super(key: key);

  final controller = Get.find<HistoryController>();

  Widget _buildHistoryCell(
      {required String title,
      required Function(bool?) onTap,
      required bool isSkip}) {
    return Card(
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: SizedBox(
        width: 355.w,
        height: 60.h,
        child: Row(
          children: [
            SizedBox(
              width: 10.w,
            ),
            Text(
              title,
              style: TextStyle(
                fontSize: 13.sp,
              ),
            ),
            const Spacer(),
            Checkbox(
              value: isSkip,
              onChanged: onTap,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildHistory(int index) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        child: Column(
          children: index == 0
              ? [
                  ...controller.level1.map(
                    (e) => _buildHistoryCell(
                      title: e.word.word,
                      onTap: (value) {
                        controller.handleIsSkipCell(
                            controller.level1, e.word.word, value!);
                      },
                      isSkip: e.isSkip,
                    ),
                  ),
                  SizedBox(
                    height: 100.h,
                  ),
                ]
              : index == 1
                  ? [
                      ...controller.level2.map(
                        (e) => _buildHistoryCell(
                          title: e.word.word,
                          onTap: (value) {
                            controller.handleIsSkipCell(
                                controller.level2, e.word.word, value!);
                          },
                          isSkip: e.isSkip,
                        ),
                      ),
                      SizedBox(
                        height: 100.h,
                      ),
                    ]
                  : index == 2
                      ? [
                          ...controller.level3.map(
                            (e) => _buildHistoryCell(
                              title: e.word.word,
                              onTap: (value) {
                                controller.handleIsSkipCell(
                                    controller.level3, e.word.word, value!);
                              },
                              isSkip: e.isSkip,
                            ),
                          ),
                          SizedBox(
                            height: 100.h,
                          ),
                        ]
                      : index == 3
                          ? [
                              ...controller.level4.map(
                                (e) => _buildHistoryCell(
                                  title: e.word.word,
                                  onTap: (value) {
                                    controller.handleIsSkipCell(
                                        controller.level4, e.word.word, value!);
                                  },
                                  isSkip: e.isSkip,
                                ),
                              ),
                              SizedBox(
                                height: 100.h,
                              ),
                            ]
                          : [
                              ...controller.level5.map(
                                (e) => _buildHistoryCell(
                                  title: e.word.word,
                                  onTap: (value) {
                                    controller.handleIsSkipCell(
                                        controller.level5, e.word.word, value!);
                                  },
                                  isSkip: e.isSkip,
                                ),
                              ),
                              SizedBox(
                                height: 100.h,
                              ),
                            ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<HistoryController>();

    return SafeArea(
      child: Obx(
        () {
          return DefaultTabController(
            length: 5,
            child: Scaffold(
              appBar: transparentAppBar(
                leadingCallback: () => controller.handleNavBack(),
                title: "Lịch sử ôn luyện từ vựng",
                actions: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.more_vert,
                      size: 18.w,
                      color: AppColors.thirdElement,
                    ),
                  ),
                ],
                bottom: TabBar(
                  onTap: (index) {
                    // print(index);
                  },
                  isScrollable: true,
                  tabs: [
                    Tab(
                      child: Text(
                        "Cấp độ 1",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Cấp độ 2",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Cấp độ 3",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Cấp độ 4",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Cấp độ 5",
                        style: TextStyle(
                          fontSize: 13.sp,
                        ),
                      ),
                    ),
                  ],
                  indicator: DotIndicator(
                    color: Colors.black,
                    distanceFromCenter: 20.h,
                    radius: 3.w,
                    paintingStyle: PaintingStyle.fill,
                  ),
                ),
              ),
              floatingActionButton: btnFlatButtonWidget(
                onPressed: () {
                  controller.skipWords();
                },
                width: 325.w,
                fontWeight: FontWeight.w600,
                title: "Lưu thay đổi",
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              body: TabBarView(
                children: [
                  _buildHistory(0),
                  _buildHistory(1),
                  _buildHistory(2),
                  _buildHistory(3),
                  _buildHistory(4),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
