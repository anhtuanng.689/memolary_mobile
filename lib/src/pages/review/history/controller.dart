import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/networks/word_skip_request.dart';
import 'package:memolary_mobile/src/pages/review/review/controller.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import '../../../datas/models/word_review.dart';
import '../../../datas/providers/review_provider.dart';
import '../../../routes/index.dart';

class HistoryController extends GetxController {
  final level1 = [].obs;
  final level2 = [].obs;
  final level3 = [].obs;
  final level4 = [].obs;
  final level5 = [].obs;

  final wordsSkip = <WordSkip>[].obs;

  final ReviewController reviewController = Get.find();

  @override
  void onInit() {
    prepareData();
    super.onInit();
  }

  @override
  void dispose() {
    resetData();
    super.dispose();
  }

  void resetData() {
    print("reseting");
    level1.clear();
    level2.clear();
    level3.clear();
    level4.clear();
    level5.clear();
    wordsSkip.clear();
    reviewController.prepareData();
  }

  void handleNavBack() {
    resetData();
    Get.offAndToNamed(AppRoutes.dashboard);
  }

  void prepareData() async {
    reviewController.fetchAllWordsReview();

    final review = reviewController.review;
    // final review = Get.arguments[0] as Rx<LevelResponse?>;

    level1.value = review.value!.level.level1;
    level2.value = review.value!.level.level2;
    level3.value = review.value!.level.level3;
    level4.value = review.value!.level.level4;
    level5.value = review.value!.level.level5;
  }

  void skipWords() async {
    try {
      if (wordsSkip.isEmpty) {
        toastInfo(msg: "Hãy chọn từ vựng để bỏ qua ôn tập");
        return;
      }

      await ReviewProvider().skipWords(
        WordSkipRequest(words: wordsSkip),
      );
      toastInfo(msg: "Lưu thay đổi thành công");
      wordsSkip.clear();
    } catch (e) {
      print(e);
      toastInfo(msg: "Đã có lỗi xảy ra");
    }
  }

  handleIsSkipCell(RxList level, String word, bool check) {
    print("Ok $word $check");
    int index = -1;
    for (int i = 0; i < level.length; i++) {
      print(level[i].word.word);
      if (level[i].word.word == word) {
        index = i;
        break;
      }
    }
    print("index $index ${level[index].isSkip}");
    if (index == -1) {
      return;
    } else {
      bool isFound = false;
      for (int i = 0; i < wordsSkip.length; i++) {
        if (wordsSkip[i].word == level[index].word.id) {
          isFound = true;
          var temp = WordSkip(word: level[index].word.id, isSkip: check);
          wordsSkip[i] = temp;
          break;
        }
      }
      if (!isFound) {
        wordsSkip.add(
          WordSkip(word: level[index].word.id, isSkip: check),
        );
      }
      var temp = WordReview(
          word: level[index].word,
          lastReview: level[index].lastReview,
          isSkip: check);
      level[index] = temp;
      print(level[index].isSkip);
      print(wordsSkip.length);
    }
  }
}
