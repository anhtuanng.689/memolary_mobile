import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../datas/models/word.dart';
import '../../../../values/index.dart';
import '../../../../widgets/index.dart';
import '../index.dart';

Widget _buildMatchAnswerCard({
  required VoidCallback onPressed,
  required String choice,
  required bool isToggle,
  String? image,
}) {
  return Card(
    elevation: 3,
    shape: RoundedRectangleBorder(
      borderRadius: Radii.k6pxRadius,
      side: isToggle
          ? BorderSide(color: Colors.blue, width: 3.w)
          : BorderSide.none,
    ),
    child: InkWell(
      borderRadius: Radii.k6px,
      onTap: onPressed,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            image != null
                ? Expanded(
                    child: netImageCached(
                      image,
                    ),
                  )
                : const SizedBox(),
            SizedBox(
              height: 3.h,
            ),
            Text(
              choice,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget _buildGridMatchAnswer(
    List<Word> words, MatchAnswerController controller) {
  return GridView.count(
    physics: const NeverScrollableScrollPhysics(),
    crossAxisCount: 2,
    mainAxisSpacing: 15.h,
    crossAxisSpacing: 5.w,
    shrinkWrap: true,
    children: [
      _buildMatchAnswerCard(
        choice: words[0].word,
        image: words[0].image,
        isToggle: controller.isToggleListMatchAnswer[0],
        onPressed: () {
          controller.toggleMatchAnswer(0);
        },
      ),
      _buildMatchAnswerCard(
        choice: words[1].word,
        image: words[1].image,
        isToggle: controller.isToggleListMatchAnswer[1],
        onPressed: () {
          controller.toggleMatchAnswer(1);
        },
      ),
      _buildMatchAnswerCard(
        choice: words[2].word,
        image: words[2].image,
        isToggle: controller.isToggleListMatchAnswer[2],
        onPressed: () {
          controller.toggleMatchAnswer(2);
        },
      ),
      _buildMatchAnswerCard(
        choice: words[3].word,
        image: words[3].image,
        isToggle: controller.isToggleListMatchAnswer[3],
        onPressed: () {
          controller.toggleMatchAnswer(3);
        },
      ),
    ],
  );
}

Widget buildMatchAnswerReview(
    {required List<Word> words,
    required MatchAnswerController matchAnswerController,
    required Word word}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Từ nào dưới đây có nghĩa là \"${word.wordMeaning}\"?",
        style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      _buildGridMatchAnswer(words, matchAnswerController)
    ],
  );
}
