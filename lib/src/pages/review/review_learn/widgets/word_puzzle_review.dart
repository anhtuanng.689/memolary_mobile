import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memolary_mobile/src/pages/review/review_learn/index.dart';
import '../../../../datas/models/word.dart';
import '../../../../values/index.dart';
import '../../../../widgets/index.dart';

Widget _buildWordPuzzle({required WordPuzzleController controller}) {
  return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.center,
      runSpacing: 5.h,
      children: controller.wordQuestion.isNotEmpty
          ? [
              ...controller.wordQuestion.asMap().entries.map(
                (e) {
                  int index = e.key;
                  Map<String, bool> val = e.value;
                  String character = '';
                  bool isPress = true;
                  val.forEach(
                    (key, value) {
                      character = key;
                      isPress = value;
                    },
                  );
                  return _buildCell(
                    character: character,
                    isPress: isPress,
                    isDown: true,
                    onPress: () {
                      controller.toggleCellQuestion(index,
                          controller.wordAnswer, controller.wordQuestion);
                    },
                  );
                },
              ),
            ]
          : [const SizedBox()]);
}

// answer
Widget _buildQuestionPuzzle({required WordPuzzleController controller}) {
  return Stack(
    alignment: AlignmentDirectional.topStart,
    children: [
      Column(
        children: [
          SizedBox(
            height: 50.h,
          ),
          const Divider(
            color: Colors.grey,
            thickness: 2,
          ),
          SizedBox(
            height: 45.h,
          ),
          const Divider(
            color: Colors.grey,
            thickness: 2,
          ),
          SizedBox(
            height: 45.h,
          ),
          const Divider(
            color: Colors.grey,
            thickness: 2,
          ),
          SizedBox(
            height: 30.h,
          ),
        ],
      ),
      Center(
        child: Wrap(
          children: [
            ...controller.wordAnswer.asMap().entries.map(
              (e) {
                int index = e.key;
                String character = e.value;
                return _buildCell(
                  character: character,
                  isPress: true,
                  isDown: false,
                  onPress: () {
                    controller.toggleCellAnswer(
                        index, controller.wordAnswer, controller.wordQuestion);
                  },
                );
              },
            ),
          ],
        ),
      ),
    ],
  );
}

Widget _buildCell(
    {required String character,
    required bool isPress,
    required bool isDown,
    required VoidCallback onPress}) {
  return GestureDetector(
    onTap: onPress,
    child: Card(
      color: isPress
          ? isDown
              ? AppColors.thirdElementText.withOpacity(0.1)
              : AppColors.fourthElement
          : AppColors.primaryBackground,
      elevation: 1,
      shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
      child: SizedBox(
        width: 35.w,
        height: 50.h,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),
          child: Center(
            child: Text(
              character,
              style: TextStyle(
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w600,
                  color: isPress
                      ? AppColors.primaryElementText
                      : AppColors.primaryText),
            ),
          ),
        ),
      ),
    ),
  );
}

Widget buildWordPuzzleReview(
    {required Word word, required WordPuzzleController wordPuzzleController}) {
  return Column(
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Ghép từ",
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      Text(
        word.wordMeaning,
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 40.h,
      ),
      _buildQuestionPuzzle(controller: wordPuzzleController),
      _buildWordPuzzle(controller: wordPuzzleController),
      SizedBox(
        height: 20.h,
      ),
    ],
  );
}
