import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';

import '../../../../values/index.dart';
import '../index.dart';

Widget _buildFillSentenceReviewCard({
  required VoidCallback onPressed,
  required Word word,
  required bool isToggle,
}) {
  return GestureDetector(
    onTap: onPressed,
    child: Card(
      color: isToggle ? AppColors.fourthElement : AppColors.primaryBackground,
      elevation: 3,
      shape: const RoundedRectangleBorder(
        borderRadius: Radii.k6pxRadius,
      ),
      child: SizedBox(
        width: 355.w,
        height: 70.h,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
          child: Center(
            child: Text(
              word.wordMeaning,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              style: TextStyle(
                  fontSize: 13.sp,
                  color: isToggle
                      ? AppColors.primaryElementText
                      : AppColors.primaryText),
            ),
          ),
        ),
      ),
    ),
  );
}

Widget buildFillSentenceReview({
  required List<Word> words,
  required Word word,
  required FillSentenceController fillSentenceController,
  // required String prefix,
  // required String after,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Chọn nghĩa của từ được gạch chân",
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      Card(
        elevation: 3,
        shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
        child: SizedBox(
          width: 355.w,
          height: 110.h,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: RichText(
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              text: TextSpan(
                text: word.sentence,
                style: TextStyle(
                  fontSize: 14.sp, color: AppColors.primaryText,
                  // decoration: TextDecoration.underline,
                  // fontWeight: FontWeight.w600,
                ),
              ),
              // TextSpan(
              //   text: prefix,
              //   style: TextStyle(fontSize: 14.sp, color: AppColors.primaryText),
              //   children: [
              //     TextSpan(
              //       text: word.word,
              //       style: const TextStyle(
              //         decoration: TextDecoration.underline,
              //         fontWeight: FontWeight.w600,
              //       ),
              //     ),
              //     TextSpan(text: after),
              //   ],
              // ),
            ),
          ),
        ),
      ),
      SizedBox(
        height: 30.h,
      ),
      _buildFillSentenceReviewCard(
        word: words[0],
        isToggle: fillSentenceController.isToggleListFillSentence[0],
        onPressed: () {
          fillSentenceController.toggleFillSentence(0);
        },
      ),
      SizedBox(
        height: 10.h,
      ),
      _buildFillSentenceReviewCard(
        word: words[1],
        isToggle: fillSentenceController.isToggleListFillSentence[1],
        onPressed: () {
          fillSentenceController.toggleFillSentence(1);
        },
      ),
      SizedBox(
        height: 10.h,
      ),
      _buildFillSentenceReviewCard(
        word: words[2],
        isToggle: fillSentenceController.isToggleListFillSentence[2],
        onPressed: () {
          fillSentenceController.toggleFillSentence(2);
        },
      ),
      SizedBox(
        height: 10.h,
      ),
      _buildFillSentenceReviewCard(
        word: words[3],
        isToggle: fillSentenceController.isToggleListFillSentence[3],
        onPressed: () {
          fillSentenceController.toggleFillSentence(3);
        },
      ),
    ],
  );
}
