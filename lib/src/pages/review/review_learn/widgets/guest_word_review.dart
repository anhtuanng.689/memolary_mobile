import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../datas/models/word.dart';
import '../../../../widgets/index.dart';

Widget _buildGuestWord(
    {required Word word, required TextEditingController inputController}) {
  return inputCardEdit(
    controller: inputController,
    fontSize: 18,
  );
}

Widget buildGuestWordReview(
    {required Word word, required TextEditingController guestInputController}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      SizedBox(
        height: 10.h,
      ),
      Text(
        "Điền từ",
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      Text(
        word.wordMeaning,
        style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
      ),
      SizedBox(
        height: 20.h,
      ),
      _buildGuestWord(word: word, inputController: guestInputController)
    ],
  );
}
