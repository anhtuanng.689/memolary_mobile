import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/index.dart';
import 'index.dart';

class ReviewLearnPage extends StatelessWidget {
  ReviewLearnPage({Key? key}) : super(key: key);

  final controller = Get.find<ReviewLearnController>();

  Widget _buildReview() {
    if (controller.reviewMode.value != 0) {
      if (controller.reviewMode.value == 1) {
        for(int i = 0; i < controller.randomList.length; i++) {
          print(controller.randomList[i].word);
        }
        return buildMatchAnswerReview(
            word: controller.wordQueue[0].word,
            matchAnswerController: controller.matchAnswerController,
            words: controller.randomList);

      } else if (controller.reviewMode.value == 2) {
        for(int i = 0; i < controller.randomList.length; i++) {
          print(controller.randomList[i].word);
        }
        return buildFillSentenceReview(
          word: controller.wordQueue[0].word,
          fillSentenceController: controller.fillSentenceController,
          words: controller.randomList,
          // prefix: controller.prefix.value,
          // after: controller.after.value,
        );
      } else if (controller.reviewMode.value == 3) {
        return buildGuestWordReview(
            word: controller.wordQueue[0].word,
            guestInputController:
                controller.guestWordController.guestInputController);
      } else if (controller.reviewMode.value == 4) {
        return buildSpellWordReview(
            spellInputController:
                controller.spellWordController.spellInputController,
            onPress: () => controller.speak(controller.wordQueue[0].word.word));
      } else if (controller.reviewMode.value == 5) {
        return buildWordPuzzleReview(
            word: controller.wordQueue[0].word,
            wordPuzzleController: controller.wordPuzzleController);
      } else {
        return spinKit();
      }
    } else {
      return spinKit();
    }
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ReviewLearnController>();

    return SafeArea(
      child: Obx(() {
        return GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            appBar: indicatorAppBar(
              percent: controller.wordQueue.isNotEmpty
                  ? controller.indicator.value / controller.counts.value
                  : 0,
            ),
            floatingActionButton: btnFlatButtonWidget(
              onPressed: () {
                controller.checkResult(context);
              },
              width: 325.w,
              fontWeight: FontWeight.w600,
              title: "Kiểm tra",
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            body: SingleChildScrollView(
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              physics: const NeverScrollableScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(child: _buildReview()),
                    SizedBox(
                      height: 10.h,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
