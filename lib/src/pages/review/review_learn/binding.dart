import 'package:get/get.dart';

import 'index.dart';

class ReviewLearnBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ReviewLearnController());
  }
}
