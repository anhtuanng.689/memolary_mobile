import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../datas/models/word.dart';

class MatchAnswerController extends GetxController {
  final List<bool> isToggleListMatchAnswer = [false, false, false, false].obs;

  resetMatchAnswer() {
    for (int i = 0; i < isToggleListMatchAnswer.length; i++) {
      isToggleListMatchAnswer[i] = false;
    }
  }

  bool checkResult(List words, Word question) {
    int index = isToggleListMatchAnswer.indexOf(true);
    if (index != -1) {
      if (words[index].word == question.word) {
        return true;
      }
    }
    return false;
  }

  toggleMatchAnswer(int index) {
    resetMatchAnswer();
    isToggleListMatchAnswer[index] = true;
  }

  @override
  void dispose() {
    super.dispose();
  }
}
