import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SpellWordController extends GetxController {
  final TextEditingController spellInputController = TextEditingController();

  resetSpellInput() {
    spellInputController.clear();
  }

  bool checkResult(String word, String answer) {
    if (word == answer) {
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    spellInputController.dispose();
    super.dispose();
  }
}
