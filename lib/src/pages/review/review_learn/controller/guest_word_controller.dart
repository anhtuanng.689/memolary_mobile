import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GuestWordController extends GetxController {
  final TextEditingController guestInputController = TextEditingController();

  resetGuestInput() {
    guestInputController.clear();
  }

  bool checkResult(String word, String answer) {
    if (word == answer) {
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    guestInputController.dispose();
    super.dispose();
  }
}
