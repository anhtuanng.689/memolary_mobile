import 'dart:math';
import 'package:flutter/foundation.dart';

import 'package:get/get.dart';

class WordPuzzleController extends GetxController {
  final alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

  final word = <String>[].obs;

  final wordAnswer = <String>[].obs;

  final wordQuestion = <Map<String, bool>>[].obs;

  resetList() {
    word.value = [];
    wordAnswer.value = [];
    wordQuestion.value = <Map<String, bool>>[];
  }

  bool checkResult(String word) {
    print("word: ${word.split('')}");
    print("wordAnswer: $wordAnswer");
    if (listEquals(word.split(''), wordAnswer)) {
      return true;
    }
    return false;
  }

  randomAndShuffleWord(List<String> list) {
    if (list.length < 8) {
      while (list.length < 8) {
        var random = Random().nextInt(26);
        list.add(alphabet[random]);
      }
    } else if (list.length < 12) {
      while (list.length < 12) {
        var random = Random().nextInt(26);
        list.add(alphabet[random]);
      }
    } else if (list.length < 16) {
      while (list.length < 16) {
        var random = Random().nextInt(26);
        list.add(alphabet[random]);
      }
    }
    list.shuffle();
    print(list);
  }

  preparePuzzle(String str) {
    word.value = str.split('');
    randomAndShuffleWord(word);
    for (var element in word) {
      wordQuestion.add({element: false});
    }
    print(wordQuestion);
  }

  toggleCellQuestion(int index, List<String> listAnswer,
      List<Map<String, bool>> listQuestion) {
    listQuestion[index].forEach((key, value) {
      if (value) {
        return;
      }
      listQuestion[index].update(key, (value) => true);
      listAnswer.add(key);
    });
    print(wordAnswer.length);
    print(wordQuestion.length);
  }

  toggleCellAnswer(int index, List<String> listAnswer,
      List<Map<String, bool>> listQuestion) {
    String character = listAnswer[index];
    listAnswer.removeAt(index);
    for (var map in listQuestion) {
      for (var key in map.keys) {
        if (character == key && map[key] == true) {
          map[key] = false;
          return;
        }
      }
    }
    print(wordAnswer.length);
    print(wordQuestion.length);
  }
}
