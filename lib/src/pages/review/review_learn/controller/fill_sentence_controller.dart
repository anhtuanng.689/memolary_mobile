import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../datas/models/word.dart';
import '../index.dart';

class FillSentenceController extends GetxController {
  final List<bool> isToggleListFillSentence = [false, false, false, false].obs;

  final reviewLearnController = Get.find<ReviewLearnController>();

  resetFillSentence() {
    for (int i = 0; i < isToggleListFillSentence.length; i++) {
      isToggleListFillSentence[i] = false;
    }
  }

  bool checkResult(List words, Word question) {
    int index = isToggleListFillSentence.indexOf(true);
    if (index != -1) {
      if (words[index].word == question.word) {
        return true;
      }
    }
    return false;
  }

  toggleFillSentence(int index) {
    resetFillSentence();
    isToggleListFillSentence[index] = true;
  }

  @override
  void dispose() {
    super.dispose();
  }
}
