import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';
import 'package:memolary_mobile/src/datas/networks/level_request.dart';
import 'package:memolary_mobile/src/datas/networks/review_request.dart';
import 'package:memolary_mobile/src/pages/review/review_learn/index.dart';
import 'package:memolary_mobile/src/routes/app_routes.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../datas/models/word_review.dart';
import '../../../../datas/networks/level_response.dart';
import '../../../../datas/providers/review_provider.dart';
import '../../../../values/colors.dart';
import '../../../../widgets/index.dart';
import '../../review/index.dart';

class ReviewLearnController extends GetxController {
  final LevelRequest right =
      LevelRequest(level1: [], level2: [], level3: [], level4: [], level5: []);
  final LevelRequest wrong =
      LevelRequest(level1: [], level2: [], level3: [], level4: [], level5: []);

  final rightWords = [].obs;
  final wrongWords = [].obs;

  final RxInt reviewMode = 0.obs;

  final reviewLearn = Rx<LevelResponse?>(null);

  final learn = <WordReview>[].obs;

  final randomList = <Word>[].obs;

  final indicator = 0.obs;

  final wordQueue = <WordReview>[].obs;

  final countWords = [0, 0, 0, 0, 0].obs;

  Random random = Random();

  final counts = 0.obs;

  late final wordPuzzleController = Get.put(WordPuzzleController());
  late final guestWordController = Get.put(GuestWordController());
  late final matchAnswerController = Get.put(MatchAnswerController());
  late final spellWordController = Get.put(SpellWordController());
  late final fillSentenceController = Get.put(FillSentenceController());

  final FlutterTts flutterTts = FlutterTts();

  // final prefix = "".obs;
  //
  // final after = "".obs;

  final isShowMeaning = false.obs;

  final reviewController = Get.find<ReviewController>();

  // cutString(Word word) {
  //   var index = word.sentence.indexOf(word.word);
  //
  //   if (word.sentence.length > 5) {
  //     prefix.value = word.sentence.substring(0, index);
  //     after.value =
  //         word.sentence.substring(index + 1, word.sentence.length - 1);
  //   } else {
  //     prefix.value = "";
  //     after.value = "";
  //   }
  // }

  @override
  void onInit() {
    fetchReview();
    setupTts();

    ever(reviewMode, (value) {
      if (reviewMode.value == 4) {
        speak(wordQueue[0].word.word);
      } else if (reviewMode.value == 1 || reviewMode.value == 2) {
        shuffleRandomList();
      }
    });
    super.onInit();
  }

  shuffleRandomList() {
    randomList.clear();
    randomList.add(wordQueue[0].word);
    while (randomList.length < 4) {
      int rand = random.nextInt(learn.length);
      if (!randomList.contains(learn[rand].word)) {
        randomList.add(learn[rand].word);
      }
    }
    randomList.shuffle();
    print(randomList);
  }

  setupTts() async {
    await flutterTts.setLanguage("en-US");
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setVolume(1.0);
    await flutterTts.setPitch(0.8);
  }

  speak(String text) async {
    await flutterTts.speak(text);
  }

  fetchReview() async {
    try {
      reviewLearn.value = await ReviewProvider().getReview();
      prepareReview(reviewLearn.value!, wordQueue);
      randomMode();
    } catch (e) {
      print(e);
    }
  }

  prepareReview(LevelResponse review, RxList list) {
    addWordsToQueue(review, list);
    addWordsToQueue(review, learn);
    countWords[0] = review.level.level1.length;
    countWords[1] = review.level.level2.length;
    countWords[2] = review.level.level3.length;
    countWords[3] = review.level.level4.length;
    countWords[4] = review.level.level5.length;
    print(wordQueue);
    counts.value = countWords[0] +
        countWords[1] +
        countWords[2] +
        countWords[3] +
        countWords[4];
  }

  addWordsToQueue(LevelResponse review, RxList list) {
    for (var element in review.level.level1) {
      list.add(element);
    }
    for (var element in review.level.level2) {
      list.add(element);
    }
    for (var element in review.level.level3) {
      list.add(element);
    }
    for (var element in review.level.level4) {
      list.add(element);
    }
    for (var element in review.level.level5) {
      list.add(element);
    }
    list.shuffle();
  }

  popQueue(int index) {
    wordQueue.removeAt(index);
    print(wordQueue.length);
  }

  pushQueue(WordReview wordReview) {
    wordQueue.add(wordReview);
    print(wordQueue.length);
  }

  randomMode() {
    reviewMode.value = -1;
    print("mode: ${reviewMode.value}");
    reviewMode.value = Random().nextInt(5) + 1;
    print("-> ${reviewMode.value}");
    if (reviewMode.value == 2) {
      // cutString(wordQueue[0].word);
    } else if (reviewMode.value == 5) {
      wordPuzzleController.preparePuzzle(wordQueue[0].word.word);
    }
  }

  resetReviewMode() {
    fillSentenceController.resetFillSentence();
    guestWordController.resetGuestInput();
    matchAnswerController.resetMatchAnswer();
    spellWordController.resetSpellInput();
    wordPuzzleController.resetList();
  }

  checkResult(BuildContext context) {
    FocusManager.instance.primaryFocus?.unfocus();
    var result = false;
    if (reviewMode.value == 1) {
      if (matchAnswerController.checkResult(randomList, wordQueue[0].word)) {
        result = true;
      }
    } else if (reviewMode.value == 2) {
      if (fillSentenceController.checkResult(randomList, wordQueue[0].word)) {
        result = true;
      }
    } else if (reviewMode.value == 3) {
      if (guestWordController.checkResult(wordQueue[0].word.word,
          guestWordController.guestInputController.text)) {
        result = true;
      }
    } else if (reviewMode.value == 4) {
      if (spellWordController.checkResult(wordQueue[0].word.word,
          spellWordController.spellInputController.text)) {
        result = true;
      }
    } else if (reviewMode.value == 5) {
      if (wordPuzzleController.checkResult(wordQueue[0].word.word)) {
        result = true;
      }
    }
    print("Result $result");
    if (result) {
      print("Dung roi");
      if (!wrongWords.contains(wordQueue[0].word.id)) {
        rightWords.add(wordQueue[0].word.id);
      }
      indicator.value++;
    } else {
      print("Sai roi");

      if (!wrongWords.contains(wordQueue[0].word.id)) {
        wrongWords.add(wordQueue[0].word.id);
      }
      pushQueue(wordQueue[0]);
    }
    print("right: $rightWords");
    print("wrong: $wrongWords");

    showResult(context, result);
  }

  navToNextMode() {
    print("length: ${wordQueue.length}");
    FocusManager.instance.primaryFocus?.unfocus();
    if (wordQueue.length == 1) {
      finishReview();
      return;
    }
    popQueue(0);

    resetReviewMode();
    randomMode();
  }

  showResult(BuildContext context, bool result) {
    showMaterialModalBottomSheet(
      isDismissible: false,
      context: context,
      builder: (context) => Obx(() {
        return SizedBox(
          height: 420.h,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    result
                        ? "assets/images/right.png"
                        : "assets/images/wrong.png",
                    scale: 5,
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                wordQueue[0].word.word,
                                style: TextStyle(
                                    fontSize: 20.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                              Text(
                                "/${wordQueue[0].word.pronounce}/",
                                style: GoogleFonts.arimo(
                                  textStyle: TextStyle(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                              Text(
                                wordQueue[0].word.wordMeaning,
                                style: TextStyle(fontSize: 13.sp),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                              Text(
                                wordQueue[0].word.sentence,
                                style: TextStyle(fontSize: 13.sp),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                              Text(
                                wordQueue[0].word.sentenceMeaning,
                                style: TextStyle(
                                    fontSize: 13.sp,
                                    color: isShowMeaning.value
                                        ? AppColors.primaryText
                                        : Colors.transparent),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              RawMaterialButton(
                                constraints: BoxConstraints(
                                    minWidth: 40.w, minHeight: 50.h),
                                onPressed: () {
                                  speak(wordQueue[0].word.word);
                                },
                                elevation: 5,
                                fillColor: AppColors.primaryElement,
                                child: Icon(
                                  Icons.volume_up,
                                  size: 26.w,
                                  color: AppColors.primaryBackground,
                                ),
                                shape: const CircleBorder(),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              RawMaterialButton(
                                constraints: BoxConstraints(
                                    minWidth: 40.w, minHeight: 50.h),
                                onPressed: () {
                                  isShowMeaning.value = !isShowMeaning.value;
                                },
                                elevation: 5,
                                fillColor: AppColors.primaryElement,
                                child: Icon(
                                  Icons.translate_outlined,
                                  size: 26.w,
                                  color: AppColors.primaryBackground,
                                ),
                                shape: const CircleBorder(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                btnFlatButtonWidget(
                  onPressed: () {
                    Get.back();
                    sleep(const Duration(milliseconds: 300),);
                    navToNextMode();
                    // toggleNext(result);
                    isShowMeaning.value = false;
                  },
                  width: 325.w,
                  fontWeight: FontWeight.w600,
                  title: "Tiếp tục",
                ),
                SizedBox(
                  height: 10.h,
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  pushResultToList(LevelResponse review, String wordId, LevelRequest level) {
    for (var element in review.level.level1) {
      if (element.word.id == wordId) {
        level.level1.add(wordId);
        return;
      }
    }
    for (var element in review.level.level2) {
      if (element.word.id == wordId) {
        level.level2.add(wordId);
        return;
      }
    }
    for (var element in review.level.level3) {
      if (element.word.id == wordId) {
        level.level3.add(wordId);
        return;
      }
    }
    for (var element in review.level.level4) {
      if (element.word.id == wordId) {
        level.level4.add(wordId);
        return;
      }
    }
    for (var element in review.level.level5) {
      if (element.word.id == wordId) {
        level.level5.add(wordId);
        return;
      }
    }
  }

  handleResultList() {
    print("--------");
    // print(right.level1);
    // print(wrong.level1);
    if (rightWords.isNotEmpty) {
      for (var id in rightWords) {
        print(id);
        pushResultToList(reviewLearn.value!, id, right);
      }
    }

    if (wrongWords.isNotEmpty) {
      for (var id in wrongWords) {
        print(id);
        pushResultToList(reviewLearn.value!, id, wrong);
      }
    }

    // print("--------");
    print(right.level1);
    print(wrong.level1);
  }

  finishReview() async {
    FocusManager.instance.primaryFocus?.unfocus();
    handleResultList();
    await ReviewProvider().updateReview(
      ReviewRequest(right: right, wrong: wrong),
    );
    Get.offAllNamed(AppRoutes.dashboard);
  }

  @override
  void dispose() {
    fillSentenceController.dispose();
    spellWordController.dispose();
    matchAnswerController.dispose();
    guestWordController.dispose();
    wordPuzzleController.dispose();

    super.dispose();
  }
}
