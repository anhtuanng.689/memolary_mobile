// import 'package:chart_components/bar_chart_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../datas/models/user.dart';
import '../../../values/index.dart';
import '../../../widgets/index.dart';
import 'index.dart';

class ReviewPage extends StatelessWidget {
  ReviewPage({Key? key}) : super(key: key);

  final controller = Get.find<ReviewController>();

  Widget _buildSapo(User user) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Xin chào, ${user.name}",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 22.sp,
          ),
        ),
        SizedBox(
          height: 5.h,
        ),
        controller.isError.value
            ? Text(
                "",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16.sp,
                ),
              )
            : GestureDetector(
                onTap: () => {controller.handleNavHistory()},
                child: Text(
                  "Các hộp từ vựng của bạn chứa ${controller.countWords[0] + controller.countWords[1] + controller.countWords[2] + controller.countWords[3] + controller.countWords[4]} từ",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16.sp,
                  ),
                ),
              ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ReviewController>();

    final color = [
      Colors.red.shade500,
      Colors.orange,
      Colors.amberAccent,
      Colors.blue,
      Colors.green
    ];

    List<String> labels = ['1', '2', '3', '4', '5'];

    return SafeArea(
      child: Obx(() {
        return Scaffold(
          body: RefreshIndicator(
            onRefresh: () async {
              await controller.prepareData();
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    controller.profileController.user.value != null
                        ? _buildSapo(controller.profileController.user.value!)
                        : spinKit(),
                    controller.isError.value ||
                            (controller.countWords[0] == 0 &&
                                controller.countWords[1] == 0 &&
                                controller.countWords[2] == 0 &&
                                controller.countWords[3] == 0 &&
                                controller.countWords[4] == 0)
                        ? Column(
                            children: [
                              SizedBox(
                                height: 80.h,
                              ),
                              emptyWidget(
                                  text: "Chưa có từ vựng nào để ôn tập 😢"),
                              SizedBox(
                                height: 80.h,
                              ),
                            ],
                          )
                        : Column(
                            children: [
                              SizedBox(
                                height: 20.h,
                              ),
                              AspectRatio(
                                aspectRatio: 2 / 2.4,
                                child: BarChart(
                                  data: controller.countWords,
                                  labels: labels,
                                  labelStyle: TextStyle(fontSize: 13.sp),
                                  valueStyle: TextStyle(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w600),
                                  displayValue: true,
                                  lineGridColor: Colors.transparent,
                                  colorList: color,
                                  // getIcon: DataRepository.getIcon,
                                  barWidth: 62.w,
                                  // barSeparation: 12,
                                  animationDuration:
                                      const Duration(milliseconds: 1000),
                                  itemRadius: 90,
                                  // iconHeight: 24,
                                  footerHeight: 25,
                                  headerValueHeight: 25,
                                  roundValuesOnText: true,
                                ),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              controller.countWordsReviewLearn[0] >= 0 ||
                                      controller.countWordsReviewLearn[1] >=
                                          0 ||
                                      controller.countWordsReviewLearn[2] >=
                                          0 ||
                                      controller.countWordsReviewLearn[3] >=
                                          0 ||
                                      controller.countWordsReviewLearn[4] >= 0
                                  ? Text(
                                      "Số từ cần ôn tập hiện tại: ${controller.countWordsReviewLearn[0] + controller.countWordsReviewLearn[1] + controller.countWordsReviewLearn[2] + controller.countWordsReviewLearn[3] + controller.countWordsReviewLearn[4]} từ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 13.sp,
                                      ),
                                    )
                                  : Text(
                                      "",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 13.sp,
                                      ),
                                    ),
                              SizedBox(
                                height: 10.h,
                              ),
                              controller.isReviewState.value
                                  ? btnFlatButtonWidget(
                                      onPressed: () {
                                        controller.handleNavReviewLearn();
                                      },
                                      width: 345.w,
                                      fontWeight: FontWeight.w600,
                                      title: "Ôn tập ngay",
                                    )
                                  : AbsorbPointer(
                                      child: btnFlatButtonWidget(
                                        onPressed: () {},
                                        width: 345.w,
                                        gbColor: AppColors.thirdElement,
                                        fontColor: AppColors.primaryElementText,
                                        title: "Chưa đến giờ ôn tập 😢",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                      ),
                                    ),
                            ],
                          ),
                    // SizedBox(
                    //   height: 20.h,
                    // ),
                    // btnFlatButtonWidget(
                    //   onPressed: () {},
                    //   width: 345.w,
                    //   gbColor: AppColors.secondaryElement,
                    //   fontColor: AppColors.primaryText,
                    //   title: "Tìm hiểu về các hộp từ vựng",
                    //   fontWeight: FontWeight.w500,
                    //   fontSize: 16,
                    // ),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
