import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/datas/providers/review_provider.dart';
import 'package:memolary_mobile/src/pages/profile/controller.dart';
import 'package:memolary_mobile/src/routes/app_routes.dart';

import '../../../datas/networks/level_response.dart';

class ReviewController extends GetxController {
  final review = Rx<LevelResponse?>(null);

  final reviewLearn = Rx<LevelResponse?>(null);

  final isError = false.obs;

  final isReviewState = false.obs;

  final countWords = [0, 0, 0, 0, 0].obs;

  final countWordsReviewLearn = [0, 0, 0, 0, 0].obs;

  final ProfileController profileController = Get.put(ProfileController());

  @override
  void onInit() {
    prepareData();
    super.onInit();
  }

  prepareData() async {
    print("running");
    isError.value = false;
    isReviewState.value = false;
    await fetchAllWordsReview();
    await fetchReview();
  }

  countAllWords(LevelResponse levelResponse, List<int> counts) async {
    counts[0] = levelResponse.level.level1.length;
    counts[1] = levelResponse.level.level2.length;
    counts[2] = levelResponse.level.level3.length;
    counts[3] = levelResponse.level.level4.length;
    counts[4] = levelResponse.level.level5.length;
  }

  fetchAllWordsReview() async {
    try {
      review.value = await ReviewProvider().getAllWordsReview();
      countAllWords(review.value!, countWords);
    } catch (e) {
      isError.value = true;
      print(e);
    }
  }

  fetchReview() async {
    try {
      reviewLearn.value = await ReviewProvider().getReview();
      countAllWords(reviewLearn.value!, countWordsReviewLearn);
      isReviewState.value = checkReviewState(countWordsReviewLearn);
    } catch (e) {
      print(e);
    }
  }

  bool checkReviewState(List<int> counts) {
    if (counts[0] == 0 &&
        counts[1] == 0 &&
        counts[2] == 0 &&
        counts[3] == 0 &&
        counts[4] == 0) {
      return false;
    }
    return true;
  }

  handleNavReviewLearn() {
    Get.toNamed(AppRoutes.reviewLearn);
  }

  handleNavHistory() {
    Get.toNamed(AppRoutes.history, arguments: [review]);
  }
}
