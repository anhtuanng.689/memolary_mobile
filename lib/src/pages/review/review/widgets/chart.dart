// import 'package:flutter/material.dart';
//
// class DataRepository {
//   static List<double> data = [];
//   static List<double> _data = [35, 54, 15, 704, 43];
//
//   static List<double> getData() {
//     data = _data;
//     return _data;
//   }
//
//   static void clearData() {
//     data = [];
//   }
//
//   static List<String> getLabels() {
//     List<String> labels = ['1', '2', '3', '4', '5'];
//
//     return labels;
//   }
//
//   static Color getColor(double value) {
//     // if (value < 2) {
//     //   return Colors.amber.shade300;
//     // } else if (value < 4) {
//     //   return Colors.amber.shade600;
//     // } else
//     return Colors.amberAccent;
//   }
//
//   static Color getColorBar(int index) {
//     switch (index) {
//       case 1:
//         return Colors.red;
//       case 2:
//         return Colors.amberAccent;
//       case 3:
//         return Colors.orange;
//       case 4:
//         return Colors.blue;
//       case 5:
//         return Colors.green;
//       default:
//         return Colors.black;
//     }
//   }
//
//   static Color getDayColor(int day) {
//     if (day < data.length) {
//       return getColor(data[day]);
//     } else
//       return Colors.indigo.shade50;
//   }
//
//   static Icon getIcon(double value) {
//     if (value < 1) {
//       return Icon(
//         Icons.star_border,
//         size: 24,
//         color: getColor(value),
//       );
//     } else if (value < 2) {
//       return Icon(
//         Icons.star_half,
//         size: 24,
//         color: getColor(value),
//       );
//     } else
//       return Icon(
//         Icons.star,
//         size: 24,
//         color: getColor(value),
//       );
//   }
// }
