// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Word _$WordFromJson(Map<String, dynamic> json) => Word(
      id: json['id'] as String,
      word: json['word'] as String,
      pronounce: json['pronounce'] as String,
      wordMeaning: json['wordMeaning'] as String,
      sentence: json['sentence'] as String,
      sentenceMeaning: json['sentenceMeaning'] as String,
      image: json['image'] as String,
      lesson: json['lesson'] as String,
      isLearned: json['isLearned'] as bool,
    );

Map<String, dynamic> _$WordToJson(Word instance) => <String, dynamic>{
      'id': instance.id,
      'word': instance.word,
      'pronounce': instance.pronounce,
      'wordMeaning': instance.wordMeaning,
      'sentence': instance.sentence,
      'sentenceMeaning': instance.sentenceMeaning,
      'image': instance.image,
      'lesson': instance.lesson,
      'isLearned': instance.isLearned,
    };
