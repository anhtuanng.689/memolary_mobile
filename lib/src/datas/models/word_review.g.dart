// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WordReview _$WordReviewFromJson(Map<String, dynamic> json) => WordReview(
      word: Word.fromJson(json['word'] as Map<String, dynamic>),
      lastReview: json['lastReview'] as String,
      isSkip: json['isSkip'] as bool,
    );

Map<String, dynamic> _$WordReviewToJson(WordReview instance) =>
    <String, dynamic>{
      'word': instance.word,
      'lastReview': instance.lastReview,
      'isSkip': instance.isSkip,
    };

WordSkip _$WordSkipFromJson(Map<String, dynamic> json) => WordSkip(
      word: json['word'] as String,
      isSkip: json['isSkip'] as bool,
    );

Map<String, dynamic> _$WordSkipToJson(WordSkip instance) => <String, dynamic>{
      'word': instance.word,
      'isSkip': instance.isSkip,
    };
