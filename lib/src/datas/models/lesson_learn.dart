import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';

part 'lesson_learn.g.dart';

@JsonSerializable()
class LessonLearn {
  late final String id;
  late final String name;
  late final List<Word> words;

  LessonLearn({
    required this.id,
    required this.name,
    required this.words,
  });

  factory LessonLearn.fromJson(Map<String, dynamic> json) =>
      _$LessonLearnFromJson(json);
}
