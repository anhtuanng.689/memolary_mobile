import 'package:json_annotation/json_annotation.dart';

part 'lesson.g.dart';

@JsonSerializable()
class Lesson {
  late final String id;
  late final String name;
  late final String course;
  late final int number;

  Lesson(
      {required this.id,
      required this.name,
      required this.course,
      required this.number});

  factory Lesson.fromJson(Map<String, dynamic> json) => _$LessonFromJson(json);
}
