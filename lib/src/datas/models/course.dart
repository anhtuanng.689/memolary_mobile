import 'package:json_annotation/json_annotation.dart';

import 'lesson.dart';

part 'course.g.dart';

@JsonSerializable()
class Course {
  late final String id;
  late final String name;
  late final String image;
  late final String description;
  late final List<Lesson> lessons;
  late final int number;
  late final bool isOwner;
  late final bool isJoined;
  late final String owner;
  late final int viewCount;

  Course(
      {required this.id,
      required this.name,
      required this.image,
      required this.description,
      required this.lessons,
      required this.number,
      required this.isOwner,
      required this.isJoined,
      required this.owner,
      required this.viewCount});

  factory Course.fromJson(Map<String, dynamic> json) => _$CourseFromJson(json);
}
