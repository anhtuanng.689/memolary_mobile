// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_course.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyCourse _$MyCourseFromJson(Map<String, dynamic> json) => MyCourse(
      id: json['id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
      number: json['number'] as int,
      description: json['description'] as String,
      viewCount: json['viewCount'] as int,
    );

Map<String, dynamic> _$MyCourseToJson(MyCourse instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'number': instance.number,
      'description': instance.description,
      'viewCount': instance.viewCount,
    };
