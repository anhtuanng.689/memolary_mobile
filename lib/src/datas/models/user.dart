import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/my_course.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  late final String email;
  late final String name;
  late final String avatar;
  late final String fcmToken;
  late final String sex;
  late final String phone;
  late final String birthday;
  late final int recallWords;
  late final String recallTime;
  late final List<MyCourse> courses;
  late final String createdAt;

  User({
    required this.email,
    required this.name,
    required this.avatar,
    required this.fcmToken,
    required this.sex,
    required this.phone,
    required this.birthday,
    required this.recallWords,
    required this.recallTime,
    required this.courses,
    required this.createdAt,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
