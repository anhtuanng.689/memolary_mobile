import 'package:json_annotation/json_annotation.dart';

part 'my_course.g.dart';

@JsonSerializable()
class MyCourse {
  late final String id;
  late final String name;
  late final String image;
  late final int number;
  late final String description;
  late final int viewCount;

  MyCourse(
      {required this.id,
      required this.name,
      required this.image,
      required this.number,
      required this.description,
      required this.viewCount});

  factory MyCourse.fromJson(Map<String, dynamic> json) =>
      _$MyCourseFromJson(json);
}
