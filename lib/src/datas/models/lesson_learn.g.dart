// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lesson_learn.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LessonLearn _$LessonLearnFromJson(Map<String, dynamic> json) => LessonLearn(
      id: json['id'] as String,
      name: json['name'] as String,
      words: (json['words'] as List<dynamic>)
          .map((e) => Word.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LessonLearnToJson(LessonLearn instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'words': instance.words,
    };
