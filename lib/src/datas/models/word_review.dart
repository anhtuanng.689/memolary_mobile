import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';

part 'word_review.g.dart';

@JsonSerializable()
class WordReview {
  late final Word word;
  late final String lastReview;
  late final bool isSkip;

  WordReview(
      {required this.word, required this.lastReview, required this.isSkip});

  factory WordReview.fromJson(Map<String, dynamic> json) =>
      _$WordReviewFromJson(json);
}

@JsonSerializable()
class WordSkip {
  late final String word;
  late final bool isSkip;

  WordSkip({required this.word, required this.isSkip});

  factory WordSkip.fromJson(Map<String, dynamic> json) =>
      _$WordSkipFromJson(json);

  Map<String, dynamic> toJson() => _$WordSkipToJson(this);
}
