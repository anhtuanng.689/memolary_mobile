// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'course.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Course _$CourseFromJson(Map<String, dynamic> json) => Course(
      id: json['id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
      description: json['description'] as String,
      lessons: (json['lessons'] as List<dynamic>)
          .map((e) => Lesson.fromJson(e as Map<String, dynamic>))
          .toList(),
      number: json['number'] as int,
      isOwner: json['isOwner'] as bool,
      isJoined: json['isJoined'] as bool,
      owner: json['owner'] as String,
      viewCount: json['viewCount'] as int,
    );

Map<String, dynamic> _$CourseToJson(Course instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'description': instance.description,
      'lessons': instance.lessons,
      'number': instance.number,
      'isOwner': instance.isOwner,
      'isJoined': instance.isJoined,
      'owner': instance.owner,
      'viewCount': instance.viewCount,
    };
