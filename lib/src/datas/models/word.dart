import 'package:json_annotation/json_annotation.dart';

part 'word.g.dart';

@JsonSerializable()
class Word {
  late final String id;
  late final String word;
  late final String pronounce;
  late final String wordMeaning;
  late final String sentence;
  late final String sentenceMeaning;
  late final String image;
  late final String lesson;
  late final bool isLearned;

  Word({
    required this.id,
    required this.word,
    required this.pronounce,
    required this.wordMeaning,
    required this.sentence,
    required this.sentenceMeaning,
    required this.image,
    required this.lesson,
    required this.isLearned,
  });

  factory Word.fromJson(Map<String, dynamic> json) => _$WordFromJson(json);
}
