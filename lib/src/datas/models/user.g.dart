// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      email: json['email'] as String,
      name: json['name'] as String,
      avatar: json['avatar'] as String,
      fcmToken: json['fcmToken'] as String,
      sex: json['sex'] as String,
      phone: json['phone'] as String,
      birthday: json['birthday'] as String,
      recallWords: json['recallWords'] as int,
      recallTime: json['recallTime'] as String,
      courses: (json['courses'] as List<dynamic>)
          .map((e) => MyCourse.fromJson(e as Map<String, dynamic>))
          .toList(),
      createdAt: json['createdAt'] as String,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'email': instance.email,
      'name': instance.name,
      'avatar': instance.avatar,
      'fcmToken': instance.fcmToken,
      'sex': instance.sex,
      'phone': instance.phone,
      'birthday': instance.birthday,
      'recallWords': instance.recallWords,
      'recallTime': instance.recallTime,
      'courses': instance.courses,
      'createdAt': instance.createdAt,
    };
