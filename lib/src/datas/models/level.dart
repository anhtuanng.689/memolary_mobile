import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/word_review.dart';
import 'package:memolary_mobile/src/datas/models/word.dart';

part 'level.g.dart';

@JsonSerializable()
class Level {
  late final List<WordReview> level1;
  late final List<WordReview> level2;
  late final List<WordReview> level3;
  late final List<WordReview> level4;
  late final List<WordReview> level5;

  Level({
    required this.level1,
    required this.level2,
    required this.level3,
    required this.level4,
    required this.level5,
  });

  factory Level.fromJson(Map<String, dynamic> json) => _$LevelFromJson(json);
}
