import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/models/lesson_learn.dart';
import 'package:memolary_mobile/src/datas/networks/level_response.dart';
import 'package:memolary_mobile/src/datas/networks/review_response.dart';
import 'package:memolary_mobile/src/datas/networks/word_skip_request.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';

import '../../values/index.dart';
import '../networks/lesson_response.dart';
import '../networks/review_request.dart';

class ReviewProvider {
  late Dio dio;

  ReviewProvider() {
    dio = DioProvider().getDio();
  }

  Future<LevelResponse> getAllWordsReview() async {
    try {
      Response response = await dio.get(baseReviewApi);
      return ReviewResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<LevelResponse> getReview() async {
    try {
      Response response = await dio.get("$baseReviewApi/review");
      return ReviewResponse.fromJson(response.data).data;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  Future<void> updateReview(ReviewRequest reviewRequest) async {
    try {
      await dio.put("$baseReviewApi/review", data: reviewRequest.toJson());
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  Future<void> skipWords(WordSkipRequest wordSkipRequest) async {
    try {
      await dio.put("$baseReviewApi/skip", data: wordSkipRequest.toJson());
    } catch (e) {
      print(e);
      rethrow;
    }
  }
}
