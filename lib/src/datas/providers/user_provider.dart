import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/networks/user_request.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';

import '../../values/index.dart';
import '../models/user.dart';
import '../networks/user_response.dart';

class UserProvider {
  late Dio dio;

  UserProvider() {
    dio = DioProvider().getDio();
  }

  Future<User> getProfile() async {
    try {
      Response response = await dio.get("$baseUserApi/profile");
      return UserResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateUser(UserRequest userRequest) async {
    try {
      print(userRequest.fcmToken);
      await dio.put("$baseUserApi/fcm", data: userRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateRecall(RecallRequest recallRequest) async {
    try {
      await dio.put("$baseUserApi/recall", data: recallRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }
}
