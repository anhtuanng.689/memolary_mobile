import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/networks/register_request.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';

import '../../values/index.dart';
import '../networks/login_request.dart';
import '../networks/login_response.dart';

class AuthProvider {
  final dio = DioProvider().getDio(handleAuth: false);

  Future<String> signIn(LoginRequest loginRequest) async {
    try {
      Response response =
          await dio.post("$baseAuthApi/login", data: loginRequest.toJson());
      return LoginResponse.fromJson(response.data).accessToken;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> register(RegisterRequest registerRequest) async {
    try {
      await dio.post("$baseAuthApi/register", data: registerRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }
}
