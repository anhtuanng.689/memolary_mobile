import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/values/index.dart';

import 'interceptors/authentication_response_interceptor.dart';
import 'interceptors/bearer_token_interceptor.dart';

class DioProvider {
  final _dio = Dio();

  Dio getDio({bool handleAuth = true}) {
    _dio.interceptors.add(AuthenticationResponseInterceptorHandler());
    if (handleAuth) {
      _dio.interceptors.add(
          BearerTokenInterceptor(accessToken: Storage().getAccessToken()!));
    }
    return _dio;
  }
}
