// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
//
// class CloudMessaging {
//   final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//
//   void registerNotification() async {
//     // 1. Initialize the Firebase app
//     // await Firebase.initializeApp();
//
//     // 2. Instantiate Firebase Messaging
//
//     // 3. On iOS, this helps to take the user permissions
//     NotificationSettings settings = await _firebaseMessaging.requestPermission(
//       alert: true,
//       badge: true,
//       provisional: false,
//       sound: true,
//     );
//
//     if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//       print('User granted permission');
//     } else {
//       print('User declined or has not accepted permission');
//     }
//   }
//
//   void _fireBaseConfig() {
//     _firebaseMessaging._firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("onMessage: $message");
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//       },
//     );
//     _firebaseMessaging.subscribeToTopic('all');
//
//     _firebaseMessaging.requestNotificationPermissions(
//         const IosNotificationSettings(sound: true, badge: true, alert: true));
//     _firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print("Settings registered: $settings");
//     });
//
//     _firebaseMessaging.getToken().then((String token) {
//       assert(token != null);
//       print('Token FCM : $token');
//     });
//   }
// }
//
// class PushNotification {
//   String? title;
//   String? body;
//
//   PushNotification({
//     this.title,
//     this.body,
//   });
// }
