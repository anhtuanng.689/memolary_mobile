import 'package:dio/dio.dart';

class BearerTokenInterceptor extends Interceptor {
  late String accessToken;

  BearerTokenInterceptor({required this.accessToken});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers["Authorization"] = "Bearer $accessToken";
    options.headers["X-RapidAPI-Host"] = "wordsapiv1.p.rapidapi.com";
    options.headers["X-RapidAPI-Key"] =
        "d369a4f7b8msh256067fde0c38eap13c55cjsn6b98195ce4b4";
    super.onRequest(options, handler);
  }
}
