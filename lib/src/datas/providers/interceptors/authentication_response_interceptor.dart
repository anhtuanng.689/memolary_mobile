import 'package:dio/dio.dart';

import '../network_exception.dart';

class AuthenticationResponseInterceptorHandler extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    switch (response.statusCode) {
      case 200:
      case 201:
      case 202:
        return super.onResponse(response, handler);
      case 400:
      case 401:
      case 403:
        throw UnauthorisedException("Wrong credentials");
      default:
        throw NetworkException("Something went wrong");
    }
  }
}
