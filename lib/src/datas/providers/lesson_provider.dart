import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/models/lesson_learn.dart';
import 'package:memolary_mobile/src/datas/networks/lesson_request.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';
import 'package:http_parser/http_parser.dart';

import '../../values/index.dart';
import '../networks/learned_word_request.dart';
import '../networks/lesson_response.dart';
import '../networks/pronunciation_response.dart';

class LessonProvider {
  late Dio dio;

  LessonProvider() {
    dio = DioProvider().getDio();
  }

  Future<LessonLearn> getLesson(String lessonId) async {
    try {
      print("$baseLessonsApi/$lessonId");
      Response response = await dio.get("$baseLessonsApi/$lessonId");
      print(response.data);
      return LessonResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> createLesson(LessonRequest lessonRequest) async {
    try {
      await dio.post(baseLessonsApi, data: lessonRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }

  Future<void> submitLearnedWords(
      String lessonId, LearnedWordRequest learnedWordRequest) async {
    try {
      await dio.post("$baseLessonsApi/$lessonId/learn",
          data: learnedWordRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }

  Future<String> getPronunciation(String word) async {
    try {
      Response response = await dio
          .get("https://wordsapiv1.p.rapidapi.com/words/$word/pronunciation");
      print(response.data);
      return PronunciationResponse.fromJson(response.data).pronunciation.all;
    } catch (e) {
      rethrow;
    }
  }
}
