import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/models/lesson_learn.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';

import '../../values/index.dart';
import '../networks/lesson_response.dart';

class LearnProvider {
  late Dio dio;

  LearnProvider() {
    dio = DioProvider().getDio();
  }

  Future<LessonLearn> getWordsToLearn(String lessonId) async {
    try {
      print("$baseLessonsApi/$lessonId");
      Response response = await dio.get("$baseLessonsApi/$lessonId/learn");
      print(response.data);
      return LessonResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }
}
