import 'package:dio/dio.dart';
import 'package:memolary_mobile/src/datas/models/my_course.dart';
import 'package:memolary_mobile/src/datas/networks/course_request.dart';
import 'package:memolary_mobile/src/datas/networks/my_course_response.dart';
import 'package:memolary_mobile/src/datas/providers/dio_provider.dart';

import '../../values/index.dart';
import '../models/course.dart';
import '../networks/course_response.dart';

class CourseProvider {
  late Dio dio;

  CourseProvider() {
    dio = DioProvider().getDio();
  }

  Future<void> createCourse(CourseRequest courseRequest) async {
    try {
      await dio.post(baseCoursesApi, data: courseRequest.toJson());
    } catch (e) {
      rethrow;
    }
  }

  Future<List<MyCourse>> getMyCourses() async {
    try {
      Response response = await dio.get(baseCoursesApi);
      print(response.data);
      return MyCourseResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<MyCourse>> getAllCourses({String? query}) async {
    try {
      Response response = await dio.get(query != null
          ? "$baseCoursesApi/search?name=$query"
          : "$baseCoursesApi/all");
      print(response.data);
      return MyCourseResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<MyCourse>> getPopularCourses() async {
    try {
      Response response = await dio.get("$baseCoursesApi/popular");
      print(response.data);
      return MyCourseResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<Course> getCourse(String courseId) async {
    try {
      print("$baseCoursesApi/$courseId");
      Response response = await dio.get("$baseCoursesApi/$courseId");
      print(response.data);
      return CourseResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> joinCourse(String courseId) async {
    try {
      await dio.post("$baseCoursesApi/$courseId/join");
    } catch (e) {
      rethrow;
    }
  }
}
