import 'package:json_annotation/json_annotation.dart';

part 'level_request.g.dart';

@JsonSerializable()
class LevelRequest {
  late final List<String> level1;
  late final List<String> level2;
  late final List<String> level3;
  late final List<String> level4;
  late final List<String> level5;

  LevelRequest({
    required this.level1,
    required this.level2,
    required this.level3,
    required this.level4,
    required this.level5,
  });

  factory LevelRequest.fromJson(Map<String, dynamic> json) =>
      _$LevelRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LevelRequestToJson(this);
}
