// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'level_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LevelRequest _$LevelRequestFromJson(Map<String, dynamic> json) => LevelRequest(
      level1:
          (json['level1'] as List<dynamic>).map((e) => e as String).toList(),
      level2:
          (json['level2'] as List<dynamic>).map((e) => e as String).toList(),
      level3:
          (json['level3'] as List<dynamic>).map((e) => e as String).toList(),
      level4:
          (json['level4'] as List<dynamic>).map((e) => e as String).toList(),
      level5:
          (json['level5'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$LevelRequestToJson(LevelRequest instance) =>
    <String, dynamic>{
      'level1': instance.level1,
      'level2': instance.level2,
      'level3': instance.level3,
      'level4': instance.level4,
      'level5': instance.level5,
    };
