import 'package:json_annotation/json_annotation.dart';

import '../models/course.dart';

part 'course_response.g.dart';

@JsonSerializable()
class CourseResponse {
  late final Course data;

  CourseResponse({required this.data});

  factory CourseResponse.fromJson(Map<String, dynamic> json) =>
      _$CourseResponseFromJson(json);
}
