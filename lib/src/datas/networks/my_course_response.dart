import 'package:json_annotation/json_annotation.dart';

import '../models/my_course.dart';

part 'my_course_response.g.dart';

@JsonSerializable()
class MyCourseResponse {
  late final List<MyCourse> data;

  MyCourseResponse({required this.data});

  factory MyCourseResponse.fromJson(Map<String, dynamic> json) =>
      _$MyCourseResponseFromJson(json);
}
