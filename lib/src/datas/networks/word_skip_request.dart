import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/word_review.dart';

part 'word_skip_request.g.dart';

@JsonSerializable()
class WordSkipRequest {
  late final List<WordSkip> words;

  WordSkipRequest({
    required this.words,
  });

  factory WordSkipRequest.fromJson(Map<String, dynamic> json) =>
      _$WordSkipRequestFromJson(json);

  Map<String, dynamic> toJson() => _$WordSkipRequestToJson(this);
}
