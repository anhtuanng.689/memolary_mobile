// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'learned_word_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LearnedWordRequest _$LearnedWordRequestFromJson(Map<String, dynamic> json) =>
    LearnedWordRequest(
      words: (json['words'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$LearnedWordRequestToJson(LearnedWordRequest instance) =>
    <String, dynamic>{
      'words': instance.words,
    };
