// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_course_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyCourseResponse _$MyCourseResponseFromJson(Map<String, dynamic> json) =>
    MyCourseResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => MyCourse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MyCourseResponseToJson(MyCourseResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
