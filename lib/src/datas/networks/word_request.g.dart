// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WordRequest _$WordRequestFromJson(Map<String, dynamic> json) => WordRequest(
      word: json['word'] as String,
      pronounce: json['pronounce'] as String,
      wordMeaning: json['wordMeaning'] as String,
      sentence: json['sentence'] as String,
      sentenceMeaning: json['sentenceMeaning'] as String,
      image: json['image'] as String,
    );

Map<String, dynamic> _$WordRequestToJson(WordRequest instance) =>
    <String, dynamic>{
      'word': instance.word,
      'pronounce': instance.pronounce,
      'wordMeaning': instance.wordMeaning,
      'sentence': instance.sentence,
      'sentenceMeaning': instance.sentenceMeaning,
      'image': instance.image,
    };
