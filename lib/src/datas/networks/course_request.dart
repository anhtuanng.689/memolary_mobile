import 'package:json_annotation/json_annotation.dart';

part 'course_request.g.dart';

@JsonSerializable()
class CourseRequest {
  late final String name;
  late final String description;
  late final String image;

  CourseRequest(
      {required this.name, required this.description, required this.image});

  Map<String, dynamic> toJson() => _$CourseRequestToJson(this);
}
