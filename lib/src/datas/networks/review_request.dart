import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/networks/level_request.dart';

part 'review_request.g.dart';

@JsonSerializable()
class ReviewRequest {
  late final LevelRequest right;
  late final LevelRequest wrong;

  ReviewRequest({required this.right, required this.wrong});

  Map<String, dynamic> toJson() => _$ReviewRequestToJson(this);
}
