// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pronunciation_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PronunciationResponse _$PronunciationResponseFromJson(
        Map<String, dynamic> json) =>
    PronunciationResponse(
      pronunciation:
          Pronunciation.fromJson(json['pronunciation'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PronunciationResponseToJson(
        PronunciationResponse instance) =>
    <String, dynamic>{
      'pronunciation': instance.pronunciation,
    };

Pronunciation _$PronunciationFromJson(Map<String, dynamic> json) =>
    Pronunciation(
      all: json['all'] as String,
    );

Map<String, dynamic> _$PronunciationToJson(Pronunciation instance) =>
    <String, dynamic>{
      'all': instance.all,
    };
