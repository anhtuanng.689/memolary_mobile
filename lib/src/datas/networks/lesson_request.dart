import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/networks/word_request.dart';

import '../models/word.dart';

part 'lesson_request.g.dart';

@JsonSerializable(explicitToJson: true)
class LessonRequest {
  late final String name;
  late final String course;
  late final List<WordRequest> words;

  LessonRequest(
      {required this.name, required this.course, required this.words});

  Map<String, dynamic> toJson() => _$LessonRequestToJson(this);
}
