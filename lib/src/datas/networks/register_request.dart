import 'package:json_annotation/json_annotation.dart';

part 'register_request.g.dart';

@JsonSerializable()
class RegisterRequest {
  late final String email;
  late final String password;
  late final String name;

  RegisterRequest(
      {required this.email, required this.password, required this.name});

  Map<String, dynamic> toJson() => _$RegisterRequestToJson(this);
}
