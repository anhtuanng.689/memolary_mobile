import 'package:json_annotation/json_annotation.dart';

import '../models/user.dart';

part 'user_response.g.dart';

@JsonSerializable()
class UserResponse {
  late final User data;

  UserResponse({required this.data});

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);
}
