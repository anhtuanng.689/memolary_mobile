// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'level_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LevelResponse _$LevelResponseFromJson(Map<String, dynamic> json) =>
    LevelResponse(
      level: Level.fromJson(json['level'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LevelResponseToJson(LevelResponse instance) =>
    <String, dynamic>{
      'level': instance.level,
    };
