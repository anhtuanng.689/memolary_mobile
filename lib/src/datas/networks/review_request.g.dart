// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewRequest _$ReviewRequestFromJson(Map<String, dynamic> json) =>
    ReviewRequest(
      right: LevelRequest.fromJson(json['right'] as Map<String, dynamic>),
      wrong: LevelRequest.fromJson(json['wrong'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ReviewRequestToJson(ReviewRequest instance) =>
    <String, dynamic>{
      'right': instance.right,
      'wrong': instance.wrong,
    };
