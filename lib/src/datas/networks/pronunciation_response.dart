import 'package:json_annotation/json_annotation.dart';

part 'pronunciation_response.g.dart';

@JsonSerializable()
class PronunciationResponse {
  late final Pronunciation pronunciation;

  PronunciationResponse({required this.pronunciation});

  factory PronunciationResponse.fromJson(Map<String, dynamic> json) =>
      _$PronunciationResponseFromJson(json);
}

@JsonSerializable()
class Pronunciation {
  late final String all;

  Pronunciation({required this.all});

  factory Pronunciation.fromJson(Map<String, dynamic> json) =>
      _$PronunciationFromJson(json);
}
