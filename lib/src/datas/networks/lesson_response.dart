import 'package:json_annotation/json_annotation.dart';

import '../models/lesson_learn.dart';
import '../models/word.dart';

part 'lesson_response.g.dart';

@JsonSerializable()
class LessonResponse {
  late final LessonLearn data;

  LessonResponse({required this.data});

  factory LessonResponse.fromJson(Map<String, dynamic> json) =>
      _$LessonResponseFromJson(json);
}
