// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserRequest _$UserRequestFromJson(Map<String, dynamic> json) => UserRequest(
      fcmToken: json['fcmToken'] as String,
    );

Map<String, dynamic> _$UserRequestToJson(UserRequest instance) =>
    <String, dynamic>{
      'fcmToken': instance.fcmToken,
    };

RecallRequest _$RecallRequestFromJson(Map<String, dynamic> json) =>
    RecallRequest(
      recallWords: json['recallWords'] as int,
      recallTime: json['recallTime'] as String,
    );

Map<String, dynamic> _$RecallRequestToJson(RecallRequest instance) =>
    <String, dynamic>{
      'recallWords': instance.recallWords,
      'recallTime': instance.recallTime,
    };
