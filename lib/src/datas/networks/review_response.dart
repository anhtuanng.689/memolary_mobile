import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/networks/level_response.dart';

part 'review_response.g.dart';

@JsonSerializable()
class ReviewResponse {
  late final LevelResponse data;

  ReviewResponse({required this.data});

  factory ReviewResponse.fromJson(Map<String, dynamic> json) =>
      _$ReviewResponseFromJson(json);
}
