import 'package:json_annotation/json_annotation.dart';

part 'learned_word_request.g.dart';

@JsonSerializable()
class LearnedWordRequest {
  late final List<String> words;

  LearnedWordRequest({required this.words});

  Map<String, dynamic> toJson() => _$LearnedWordRequestToJson(this);
}
