import 'package:json_annotation/json_annotation.dart';
import 'package:memolary_mobile/src/datas/models/level.dart';

part 'level_response.g.dart';

@JsonSerializable()
class LevelResponse {
  late final Level level;

  LevelResponse({required this.level});

  factory LevelResponse.fromJson(Map<String, dynamic> json) =>
      _$LevelResponseFromJson(json);
}
