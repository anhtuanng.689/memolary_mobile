// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_skip_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WordSkipRequest _$WordSkipRequestFromJson(Map<String, dynamic> json) =>
    WordSkipRequest(
      words: (json['words'] as List<dynamic>)
          .map((e) => WordSkip.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WordSkipRequestToJson(WordSkipRequest instance) =>
    <String, dynamic>{
      'words': instance.words,
    };
