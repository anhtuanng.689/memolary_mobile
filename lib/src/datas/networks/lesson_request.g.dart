// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lesson_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LessonRequest _$LessonRequestFromJson(Map<String, dynamic> json) =>
    LessonRequest(
      name: json['name'] as String,
      course: json['course'] as String,
      words: (json['words'] as List<dynamic>)
          .map((e) => WordRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LessonRequestToJson(LessonRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'course': instance.course,
      'words': instance.words.map((e) => e.toJson()).toList(),
    };
