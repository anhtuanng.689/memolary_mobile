import 'package:json_annotation/json_annotation.dart';

part 'word_request.g.dart';

@JsonSerializable(explicitToJson: true)
class WordRequest {
  late final String word;
  late final String pronounce;
  late final String wordMeaning;
  late final String sentence;
  late final String sentenceMeaning;
  late final String image;

  WordRequest(
      {required this.word,
      required this.pronounce,
      required this.wordMeaning,
      required this.sentence,
      required this.sentenceMeaning,
      required this.image});

  factory WordRequest.fromJson(Map<String, dynamic> json) =>
      _$WordRequestFromJson(json);

  Map<String, dynamic> toJson() => _$WordRequestToJson(this);
}
