import 'package:json_annotation/json_annotation.dart';

part 'user_request.g.dart';

@JsonSerializable(explicitToJson: true)
class UserRequest {
  late final String fcmToken;

  UserRequest({required this.fcmToken});

  factory UserRequest.fromJson(Map<String, dynamic> json) =>
      _$UserRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UserRequestToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RecallRequest {
  late final int recallWords;
  late final String recallTime;

  RecallRequest({required this.recallWords, required this.recallTime});

  factory RecallRequest.fromJson(Map<String, dynamic> json) =>
      _$RecallRequestFromJson(json);

  Map<String, dynamic> toJson() => _$RecallRequestToJson(this);
}
