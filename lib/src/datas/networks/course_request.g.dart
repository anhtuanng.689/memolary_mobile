// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'course_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CourseRequest _$CourseRequestFromJson(Map<String, dynamic> json) =>
    CourseRequest(
      name: json['name'] as String,
      description: json['description'] as String,
      image: json['image'] as String,
    );

Map<String, dynamic> _$CourseRequestToJson(CourseRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'image': instance.image,
    };
