import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';

class Storage {
  final _getStorage = GetStorage();
  final darkMode = 'isDarkMode';
  final accessToken = 'accessToken';
  final firstOpen = 'isFirstOpen';
  final isLogined = 'isLogined';

  void saveIsLogined() {
    _getStorage.write(isLogined, true);
  }

  void removeIsLogined() {
    _getStorage.remove(isLogined);
  }

  bool? getIsLogined() {
    return _getStorage.read(isLogined);
  }

  void saveFirstOpen() {
    _getStorage.write(firstOpen, false);
  }

  bool? getFirstOpen() {
    return _getStorage.read(firstOpen);
  }

  String? getAccessToken() {
    return _getStorage.read(accessToken);
  }

  void saveAccessToken(String token) {
    _getStorage.write(accessToken, token);
  }

  void removeAccessToken() {
    _getStorage.remove(accessToken);
  }

  bool isAccessToken(String token) {
    print("bla bla : ${getAccessToken()}");
    return getAccessToken() == token;
  }

  ThemeMode getThemeMode() {
    return isSavedDarkMode() ? ThemeMode.dark : ThemeMode.light;
  }

  bool isSavedDarkMode() {
    return _getStorage.read(darkMode) ?? false;
  }

  void saveThemeMode(bool isDarkMode) {
    _getStorage.write(darkMode, isDarkMode);
  }

  void changeThemeMode() {
    Get.changeThemeMode(isSavedDarkMode() ? ThemeMode.light : ThemeMode.dark);
    saveThemeMode(!isSavedDarkMode());
  }
}
