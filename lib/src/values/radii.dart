import 'package:flutter/rendering.dart';

class Radii {
  static const BorderRadiusGeometry k6pxRadius =
      BorderRadius.all(Radius.circular(15));
  static const BorderRadiusGeometry k54pxRadius =
      BorderRadius.all(Radius.circular(54));

  static BorderRadius k6px = BorderRadius.circular(15);
}
