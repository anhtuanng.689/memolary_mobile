import 'dart:ui';

class AppColors {
  static const Color primaryBackground = Color.fromARGB(255, 255, 255, 255);

  static const Color primaryText = Color.fromARGB(255, 45, 45, 47);

  // static const Color primaryElement = Color.fromARGB(255, 122, 198, 37);
  static const Color primaryElement = Color.fromARGB(255, 41, 103, 255);

  static const Color fifthElement = Color.fromARGB(255, 125, 148, 241);

  static const Color fourthElement = Color.fromARGB(255, 156, 222, 91);

  static const Color primaryElementText = Color.fromARGB(255, 255, 255, 255);

  static const Color secondaryElement = Color.fromARGB(255, 246, 246, 246);

  static const Color secondaryElementText = Color.fromARGB(255, 41, 103, 255);

  static const Color thirdElement = Color.fromARGB(255, 45, 45, 47);

  static const Color thirdElementText = Color.fromARGB(255, 141, 141, 142);

  static const Color tabBarElement = Color.fromARGB(255, 208, 208, 208);

  static const Color tabCellSeparator = Color.fromARGB(255, 230, 230, 231);
}
