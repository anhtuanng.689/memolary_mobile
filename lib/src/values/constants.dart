// BASE_API
const baseApi = "http://192.168.192.167:3000";
const baseUserApi = "$baseApi/user";
const baseAuthApi = "$baseApi/auth";
const baseCoursesApi = "$baseApi/courses";
const baseLessonsApi = "$baseApi/lessons";
const baseReviewApi = "$baseApi/reviews";

// LEARN_MODE
const flashCardMode = 0;
const multipleChoiceMode = 1;
const listenMode = 2;

// FIREBASE
const storagePath = 'files/';
