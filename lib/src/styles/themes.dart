import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'theme_config.dart';

// mPlusRounded1cTextTheme
class Themes {
  final lightTheme = ThemeConfig.createTheme(
    brightness: Brightness.light,
    textTheme: GoogleFonts.lexendDecaTextTheme(),
  );
  final darkTheme = ThemeConfig.createTheme(
    brightness: Brightness.dark,
    textTheme: GoogleFonts.lexendDecaTextTheme(),
  );
}
