import 'package:flutter/material.dart';

import '../values/index.dart';

class ThemeConfig {
  static ThemeData createTheme({
    required Brightness brightness,
    required TextTheme textTheme,
  }) {
    return ThemeData(
        brightness: brightness,
        textTheme: textTheme,
        primaryColor: AppColors.primaryBackground);
  }
}
