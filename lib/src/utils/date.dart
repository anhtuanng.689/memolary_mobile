import 'package:intl/intl.dart';

String TimeFormat(DateTime dt) {
  var now = DateTime.now();
  var difference = now.difference(dt);

  if (difference.inHours < 24) {
    return "${difference.inHours} hours ago";
  } else if (difference.inDays < 30) {
    return "${difference.inDays} days ago";
  } else if (difference.inDays < 365) {
    final dtFormat = new DateFormat('MM-dd');
    return dtFormat.format(dt);
  } else {
    final dtFormat = new DateFormat('yyyy-MM-dd');
    var str = dtFormat.format(dt);
    return str;
  }
}
