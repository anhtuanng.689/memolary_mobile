class AppRoutes {
  static const welcome = '/';
  static const signIn = '/sign_in';
  static const signUp = '/sign_up';
  static const notFound = '/not_found';

  static const dashboard = '/dashboard';
  static const review = '/review';
  static const courses = '/courses';
  static const search = '/discover';
  static const profile = '/profile';

  static const course = '/courses/:course';

  static const lesson = '/lessons/:lesson';
  static const createLesson = '/lessons/create';
  static const viewWord = '/lessons/:lesson/view';
  static const learn = '/lessons/:lesson/learn';

  static const reviewLearn = '/review/learn';
  static const history = '/review/history';

  static const userDetail = '/profile/detail';
  static const achievement = '/profile/achievement';
  static const recall = '/profile/recall';
}
