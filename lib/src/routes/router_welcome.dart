import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../values/index.dart';
import 'index.dart';

class RouteWelcomeMiddleware extends GetMiddleware {
  @override
  int? priority = 0;

  RouteWelcomeMiddleware({required this.priority});

  @override
  RouteSettings? redirect(String? route) {
    if (Storage().getFirstOpen() == null) {
      return null;
    } else if (Storage().getAccessToken() != null) {
      return const RouteSettings(name: AppRoutes.dashboard);
    } else {
      return const RouteSettings(name: AppRoutes.signIn);
    }
  }
}
