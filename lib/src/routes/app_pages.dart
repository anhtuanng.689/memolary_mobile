import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../pages/courses/course/index.dart';
import '../pages/courses/courses/index.dart';
import '../pages/courses/create_lesson/index.dart';
import '../pages/courses/learn/index.dart';
import '../pages/courses/lesson/index.dart';
import '../pages/courses/view_word/index.dart';
import '../pages/discover/index.dart';
import '../pages/frame/sign_in/index.dart';
import '../pages/frame/sign_up/index.dart';
import '../pages/frame/dashboard/index.dart';

import '../pages/frame/welcome/index.dart';
import '../pages/profile/achievement/index.dart';
import '../pages/profile/index.dart';
import '../pages/profile/recall/index.dart';
import '../pages/profile/user_detail/index.dart';
import '../pages/review/history/index.dart';
import '../pages/review/review/index.dart';
import '../pages/review/review_learn/index.dart';
import 'index.dart';

// ignore: avoid_classes_with_only_static_members
class AppPages {
  static const initial = AppRoutes.welcome;
  static final RouteObserver<Route> observer = RouteObservers();
  static List<String> history = [];

  static final List<GetPage> routes = [
    GetPage(
      name: initial,
      page: () => const WelcomePage(),
      binding: WelcomeBinding(),
      middlewares: [
        RouteWelcomeMiddleware(priority: 1),
      ],
    ),
    GetPage(
      name: AppRoutes.signIn,
      page: () => const SignInPage(),
      binding: SignInBinding(),
    ),
    GetPage(
      name: AppRoutes.signUp,
      page: () => const SignUpPage(),
      binding: SignUpBinding(),
    ),
    GetPage(
        name: AppRoutes.dashboard,
        page: () => const DashboardPage(),
        bindings: [
          DashboardBinding(),
          ReviewBinding(),
          CoursesBinding(),
          DiscoverBinding(),
          ProfileBinding()
        ]),
    GetPage(
      name: AppRoutes.review,
      page: () => ReviewPage(),
      binding: ReviewBinding(),
    ),
    GetPage(
      name: AppRoutes.courses,
      page: () => CoursesPage(),
      binding: CoursesBinding(),
    ),
    GetPage(
      name: AppRoutes.search,
      page: () => DiscoverPage(),
      binding: DiscoverBinding(),
    ),
    GetPage(
      name: AppRoutes.profile,
      page: () => ProfilePage(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: AppRoutes.course,
      page: () => CoursePage(),
      binding: CourseBinding(),
    ),
    GetPage(
      name: AppRoutes.createLesson,
      page: () => CreateLessonPage(),
      binding: CreateLessonBinding(),
    ),
    GetPage(
      name: AppRoutes.lesson,
      page: () => LessonPage(),
      binding: LessonBinding(),
    ),
    GetPage(
      name: AppRoutes.viewWord,
      page: () => ViewWordPage(),
      binding: ViewWordBinding(),
    ),
    GetPage(
      name: AppRoutes.learn,
      page: () => LearnPage(),
      binding: LearnBinding(),
    ),
    GetPage(
      name: AppRoutes.reviewLearn,
      page: () => ReviewLearnPage(),
      binding: ReviewLearnBinding(),
    ),
    GetPage(
      name: AppRoutes.userDetail,
      page: () => UserDetailPage(),
      binding: UserDetailBinding(),
    ),
    GetPage(
      name: AppRoutes.achievement,
      page: () => AchievementPage(),
      binding: AchievementBinding(),
    ),
    GetPage(
      name: AppRoutes.recall,
      page: () => RecallPage(),
      binding: RecallBinding(),
    ),
    GetPage(
      name: AppRoutes.history,
      page: () => HistoryPage(),
      binding: HistoryBinding(),
    )
  ];
}
