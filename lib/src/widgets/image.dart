import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:memolary_mobile/src/widgets/index.dart';

import '../values/index.dart';

Widget netImageCached(
  String url, {
  // double? width,
  // double? height,
  // double? scale,
  BoxFit fit = BoxFit.cover,
  EdgeInsetsGeometry? margin,
}) {
  return CachedNetworkImage(
    imageUrl: url,
    imageBuilder: (context, imageProvider) => Container(
      // height: height!.h,
      // width: width!.w,
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: Radii.k6pxRadius,
        image: DecorationImage(
          image: imageProvider,
          fit: fit,
          // scale: scale!,
          // colorFilter: ColorFilter.mode(Colors.red, BlendMode.colorBurn),
        ),
      ),
    ),
    placeholder: (context, url) {
      return Container(
        alignment: Alignment.center,
        child: spinKit(),
      );
    },
    errorWidget: (context, url, error) => Icon(
      Icons.error,
      size: 18.w,
    ),
  );
}
