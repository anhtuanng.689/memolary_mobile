import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../values/index.dart';
import 'index.dart';

Widget verCard(
    {required VoidCallback onPressed,
    double width = 180,
    double height = 200,
    required String name,
    required String image,
    required int number}) {
  return Card(
    elevation: 3,
    shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
    child: SizedBox(
      width: width.w,
      height: height.h,
      child: InkWell(
        borderRadius: Radii.k6px,
        onTap: onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
          child: Column(
            children: [
              SizedBox(
                height: 3.h,
              ),
              Expanded(
                child: netImageCached(
                  image,
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
              Column(
                children: [
                  Text(
                    name,
                    style:
                        TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Text(
                    "$number từ",
                    style: TextStyle(
                      fontSize: 13.sp,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    ),
  );
}

Widget horCard(
    {required VoidCallback onPressed,
    double width = 355,
    double height = 170,
    required String name,
    required String image,
    required int number}) {
  return Card(
    elevation: 3,
    shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
    child: SizedBox(
      width: width.w,
      height: height.h,
      child: InkWell(
        borderRadius: Radii.k6px,
        onTap: onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 10.h),
          child: Row(
            children: [
              SizedBox(
                width: 10.w,
              ),
              Expanded(
                child: netImageCached(
                  image,
                ),
              ),
              SizedBox(
                width: 15.w,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                          fontSize: 16.sp, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Text(
                      "$number từ",
                      style: TextStyle(
                        fontSize: 13.sp,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}
