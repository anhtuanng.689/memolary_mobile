import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../values/index.dart';
import 'index.dart';

AppBar transparentAppBar({
  String? title,
  Widget? leading,
  VoidCallback? leadingCallback,
  List<Widget>? actions,
  PreferredSizeWidget? bottom,
}) {
  return AppBar(
    backgroundColor: AppColors.primaryBackground,
    elevation: 3,
    title: Center(
        child: title != null
            ? Text(
                title,
                style: TextStyle(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w600,
                  color: AppColors.thirdElement,
                ),
              )
            : null),
    leading: leading ??
        IconButton(
          onPressed: () {
            if (leadingCallback != null) {
              leadingCallback();
            }
            Get.back();
          },
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            size: 18.w,
            color: AppColors.thirdElement,
          ),
        ),
    actions: actions ??
        [
          AbsorbPointer(
            absorbing: true,
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.more_vert,
                size: 18.w,
                color: Colors.transparent,
              ),
            ),
          ),
        ],
    bottom: bottom,
  );
}

AppBar indicatorAppBar({Widget? leading, double percent = 0}) {
  return AppBar(
    backgroundColor: AppColors.primaryBackground,
    elevation: 3,
    leading: Row(
      children: [
        IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            size: 18.w,
            color: AppColors.thirdElement,
          ),
        ),
      ],
    ),
    actions: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: SizedBox(
          width: 310.w,
          child: LinearPercentIndicator(
            widgetIndicator: netImageCached(
              "https://cdn-icons-png.flaticon.com/512/2371/2371561.png",
            ),
            animation: true,
            animationDuration: 1000,
            animateFromLastPercent: true,
            lineHeight: 20.h,
            percent: percent,
            progressColor: AppColors.primaryElement,
            barRadius: const Radius.circular(15),
          ),
        ),
      ),
    ],
  );
}
