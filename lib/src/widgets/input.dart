import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../values/index.dart';

Widget inputTextEdit(
    {TextEditingController? controller,
    TextInputType keyboardType = TextInputType.text,
    String? hintText,
    Function(String)? onChanged,
    bool isPassword = false,
    bool isObscure = false,
    double marginTop = 10,
    bool autofocus = false,
    double fontSize = 18}) {
  return isPassword
      ? Container(
          height: 50.h,
          margin: EdgeInsets.only(top: marginTop.h),
          decoration: const BoxDecoration(
            color: AppColors.secondaryElement,
            borderRadius: Radii.k54pxRadius,
          ),
          child: TextField(
            onChanged: onChanged,
            autofocus: autofocus,
            controller: controller,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              hintText: hintText,
              contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
              border: InputBorder.none,
            ),
            style: TextStyle(
              color: AppColors.primaryText,
              fontWeight: FontWeight.w400,
              fontSize: fontSize.sp,
            ),
            maxLines: 1,
            autocorrect: false,
            obscureText: isPassword,
          ),
        )
      : Container(
          height: 50.h,
          margin: EdgeInsets.only(top: marginTop.h),
          decoration: const BoxDecoration(
            color: AppColors.secondaryElement,
            borderRadius: Radii.k54pxRadius,
          ),
          child: TextField(
            onChanged: onChanged,
            autofocus: autofocus,
            controller: controller,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              hintText: hintText,
              contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
              border: InputBorder.none,
            ),
            style: TextStyle(
              color: AppColors.primaryText,
              fontWeight: FontWeight.w400,
              fontSize: fontSize.sp,
            ),
            maxLines: 1,
            autocorrect: false,
          ),
        );
}

Widget inputCardEdit({
  TextEditingController? controller,
  TextInputType keyboardType = TextInputType.text,
  String? hintText,
  String? labelText,
  Color? color,
  bool isPassword = false,
  bool isObscure = false,
  bool autofocus = false,
  double fontSize = 13,
  double minFontSize = 13,
  double maxFontSize = 25,
  int minLines = 1,
  int maxLines = 1,
}) {
  return Card(
    color: color,
    shape: const RoundedRectangleBorder(borderRadius: Radii.k6pxRadius),
    child: AutoSizeTextField(
      autofocus: autofocus,
      controller: controller,
      keyboardType: keyboardType,
      style: TextStyle(fontSize: fontSize.sp),
      minFontSize: minFontSize,
      maxFontSize: maxFontSize,
      minLines: minLines,
      maxLines: maxLines,
      decoration: InputDecoration(
        hintText: hintText,
        labelText: labelText,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.w),
        border: InputBorder.none,
      ),
      autocorrect: false,
    ),
  );
}

Widget inputTextEditWithBorder({
  TextEditingController? controller,
  TextInputType keyboardType = TextInputType.text,
  String? hintText,
  bool isPhonetic = false,
  void Function(String)? onChanged,
  bool isPassword = false,
  bool isObscure = false,
  bool autofocus = false,
  double fontSize = 13,
  double minFontSize = 13,
  double maxFontSize = 16,
  int minLines = 1,
  int maxLines = 1,
}) {
  return Padding(
    padding: EdgeInsets.symmetric(horizontal: 10.w),
    child: AutoSizeTextField(
      autofocus: autofocus,
      controller: controller,
      keyboardType: keyboardType,
      style: isPhonetic
          ? GoogleFonts.arimo(
              textStyle:
                  TextStyle(fontSize: fontSize.sp, fontWeight: FontWeight.w500),
            )
          : TextStyle(fontSize: fontSize.sp),
      minFontSize: minFontSize,
      maxFontSize: maxFontSize,
      minLines: minLines,
      maxLines: maxLines,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintText: hintText,
        border: const UnderlineInputBorder(),
      ),
      autocorrect: false,
    ),
  );
}
