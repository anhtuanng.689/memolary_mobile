import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../values/index.dart';

Widget spinKit() {
  return Container(
    alignment: Alignment.center,
    child: SpinKitPianoWave(
      size: 16.sp,
      color: AppColors.primaryElement,
    ),
  );
}
