import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget emptyWidget({required String text}) {
  return Center(
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 100.h),
      child: SizedBox(
        child: Column(
          children: [
            Image.asset(
              "assets/images/empty.png",
              scale: 2,
            ),
            SizedBox(
              height: 10.h,
            ),
            Text(
              text,
              style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    ),
  );
}
