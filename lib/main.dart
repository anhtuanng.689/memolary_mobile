import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:memolary_mobile/src/global.dart';
import 'package:memolary_mobile/src/langs/translation_service.dart';
import 'package:memolary_mobile/src/routes/index.dart';
import 'package:memolary_mobile/src/styles/index.dart';
import 'package:memolary_mobile/src/utils/index.dart';

Future<void> main() async {
  await Global.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: () => GetMaterialApp(
        useInheritedMediaQuery: true,
        title: 'Memolary',
        theme: Themes().lightTheme,
        debugShowCheckedModeBanner: false,
        initialRoute: AppPages.initial,
        getPages: AppPages.routes,
        builder: EasyLoading.init(),
        translations: TranslationService(),
        navigatorObservers: [AppPages.observer],
        locale: TranslationService.locale,
        fallbackLocale: const Locale('en', 'US'),
        enableLog: true,
        logWriterCallback: Logger.write,
      ),
    );
  }
}
